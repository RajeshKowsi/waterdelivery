import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker_type/image_picker_type.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/models/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/models/update_user_request.dart';
import 'package:water/myalert_dialog.dart';
import 'package:water/styles/MyAppStyles.dart';
import 'common/utils.dart';

class ProfileEditPage extends StatefulWidget {
  ProfileEditPage({Key key}) : super(key: key);
  @override
  _ProfileEditPageState createState() => new _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  LoginResponse userDetail;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<State> _state = new GlobalKey<State>();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController agentMobileController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  SharedPreferences prefs;
  File _image;
  @override
  void initState() {
    super.initState();
    getUserDetails().then((value) {});
  }

  Future<void> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      print("profile userDetail =>" + userString);
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      print("profile userDetail =>" + userDetail.toJson().toString());
      String locationDetail;
      if (userDetail.data.townShip != null) {
        locationDetail = userDetail.data.townShip;
      }

      if (userDetail.data.division != null) {
        locationDetail = (locationDetail == null)
            ? userDetail.data.division
            : locationDetail + ", " + userDetail.data.division;
      }
      locationController.text = locationDetail;
      agentMobileController.text = (userDetail.data.agentCode != null)
          ? userDetail.data.agentCode.substring(2)
          : "";
      mobileNumberController.text = userDetail.data.mobileNumber.substring(2);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop();
        return;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: null,
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(
                Icons.west_sharp,
                color: Colors.grey,
                size: 30,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 30, right: 30, top: 10, bottom: 10),
                    child: Text(
                      "Edit Account",
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                    onTap: () {
                      print("ImagePicker Should be called");
                      initGalleryPickUp(context);
                    },
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: 150,
                          height: 150,
                          margin: EdgeInsets.only(bottom: 5, top: 10),
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: EdgeInsets.all(6.0),
                            child: new CircleAvatar(
                              backgroundImage: (_image != null)
                                  ? FileImage(_image)
                                  : (userDetail.data.profileImage != null)
                                      ? NetworkImage(
                                          userDetail.data.profileImage)
                                      : AssetImage("graphics/user_profile.png"),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 120, left: 100),
                          child: FloatingActionButton(
                            backgroundColor:
                                const Color(MyAppStyles.BUTTON_THEME_END_COLOR),
                            foregroundColor: Colors.white,
                            onPressed: () {
                              print("ImagePicker Should be called");
                              initGalleryPickUp(context);
                            },
                            child: Icon(
                              Icons.add,
                              size: 50,
                            ),
                          ),
                        )
                      ],
                    )),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Text(
                          "Location",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      padding: EdgeInsets.only(right: 20),
                      child: Align(
                        alignment: Alignment.center,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter location details';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          controller: locationController,
                          style: TextStyle(
                              fontSize: 20.0, height: 1.0, color: Colors.black),
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                              hintText: '',
                              prefixStyle: TextStyle(
                                  fontSize: 20.0,
                                  height: 1.0,
                                  color: Colors.black)),
                          onFieldSubmitted: (value) {},
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Text(
                          "Agent Details",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      padding: EdgeInsets.only(right: 20),
                      child: Align(
                        alignment: Alignment.center,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty || value.length < 9) {
                              return 'Please enter valid mobile number';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.phone,
                          controller: agentMobileController,
                          style: TextStyle(
                              fontSize: 20.0, height: 1.0, color: Colors.black),
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                              hintText: '',
                              prefixText: '09 ',
                              prefixStyle: TextStyle(
                                  fontSize: 20.0,
                                  height: 1.0,
                                  color: Colors.black)),
                          onFieldSubmitted: (value) {},
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Text(
                          "Phone Number",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      padding: EdgeInsets.only(right: 10),
                      child: Align(
                        alignment: Alignment.center,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty || value.length < 9) {
                              return 'Please enter valid mobile number';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.phone,
                          enabled: false,
                          controller: mobileNumberController,
                          style: TextStyle(
                              fontSize: 20.0, height: 1.0, color: Colors.black),
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                              hintText: '',
                              prefixText: '09 ',
                              prefixStyle: TextStyle(
                                  fontSize: 20.0,
                                  height: 1.0,
                                  color: Colors.black)),
                          onFieldSubmitted: (value) {},
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      updateUserDetails(context);
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                              Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Save",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
              ],
            ),
          )),
    );
  }

  Future<void> updateUserDetails(BuildContext context) async {
    Utils.showLoadingDialog(context, _state);
    if (_image != null) {
      new UserRepository().uploadImage(_image.path).then((response) {
        print("Image Uploaded url ==>" + response.data);
        Utils.isProfileImageUpdated = true;
        updateUserInformation(context, response.data);
      });
    } else {
      updateUserInformation(context, userDetail.data.profileImage);
    }
  }

  // ignore: missing_return
  Future<void> updateUserInformation(
      BuildContext context, String profilePicUrl) async {
    Utils.showLoadingDialog(context, _state);
    prefs = await SharedPreferences.getInstance();

    UpdateUserRequest request = new UpdateUserRequest(
        mobileNumber: userDetail.data.mobileNumber,
        latitude: userDetail.data.latitude.toString(),
        longitude: userDetail.data.longitude.toString(),
        address: locationController.text.toString(),
        street: userDetail.data.street,
        profilePicture: profilePicUrl,
        division: userDetail.data.division,
        township: userDetail.data.townShip,
        userType: userDetail.data.userType.toString());

    print("updateUserProfile request ==>" + request.toJson().toString());
    new UserRepository().updateUser(request).then((response) {
      print("update user response ==>" + response.toJson().toString());
      Utils.hideLoadingDialog(context);
      if (!response.error) {
        userDetail.data.address = response.data.address;
        userDetail.data.profileImage = response.data.profilePicture;
        userDetail.data.townShip = response.data.township;
        userDetail.data.division = response.data.division;
        userDetail.data.street = response.data.street;

        String userString = jsonEncode(userDetail.toJson());
        print("UserString ==>" + userString);
        prefs.setString("userdetails", userString);
        MyAlertDialog.showMyDialog(
            context, "Water App", "User Details Updated Successfully", () {
          Navigator.pop(context);
        });
      } else {
        MyAlertDialog.showMyDialog(context, "Water App",
            "Unable to update user details, Please try again later.", () {
          Navigator.pop(context);
        });
      }
    });
  }

  initGalleryPickUp(BuildContext context) async {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ImagePickerHelper(
            // isSave: true,  //if you want to save image in directory
            size: Size(300, 300),
            onDone: (file) {
              if (file == null) {
                print(null);
              } else {
                setState(() {
                  _image = file;
                });
              }
            },
          );
        });
  }
}
