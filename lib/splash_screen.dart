import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(new Duration(seconds: 1), () {
      checkIsLogin();
    });
  }

  Future<void> checkIsLogin() async {
    String userdetails = "";
    //bool isOnboardScreenDisplayed = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userdetails = prefs.getString("userdetails");
    //isOnboardScreenDisplayed = prefs.getBool("isOnboardScreenDisplayed");
    if (userdetails != "" && userdetails != null) {
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      Navigator.pushReplacementNamed(context, '/guesthomepage');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset('graphics/applogo.png'),
      ),
    );
  }
}
