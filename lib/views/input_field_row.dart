import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InpurtFieldRow extends StatelessWidget {
  final TextEditingController controller = TextEditingController();
  final FocusNode fieldFocus = FocusNode();
  final FocusNode nextFocus;
  final String hintText;
  final TextInputType inputType;
  final bool obscureText;
  final String errorMessage;
  final String prefixText;
  final IconData fieldIcon;
  final Function fieldValidator;

  InpurtFieldRow(
      {Key key,
      this.nextFocus,
      this.hintText,
      this.inputType,
      this.obscureText,
      this.errorMessage,
      this.prefixText,
      this.fieldIcon,
      this.fieldValidator});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey.withOpacity(0.5),
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Row(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Icon(
              fieldIcon,
              color: Colors.grey,
            ),
          ),
          Container(
            height: 30.0,
            width: 1.0,
            color: Colors.grey.withOpacity(0.5),
            margin: const EdgeInsets.only(left: 00.0, right: 10.0),
          ),
          if (prefixText != null)
            Padding(
              padding: EdgeInsets.only(right: 8),
              child: Text(
                prefixText,
                style: TextStyle(
                  color: Colors.grey,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          new Expanded(
            child: TextFormField(
              validator: fieldValidator,
              focusNode: fieldFocus,
              controller: controller,
              keyboardType: inputType,
              obscureText: obscureText,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: TextStyle(color: Colors.grey),
              ),
              onFieldSubmitted: (value) {
                fieldFocus.unfocus();
                if (nextFocus != null) {
                  FocusScope.of(context).requestFocus(nextFocus);
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
