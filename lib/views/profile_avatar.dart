import 'package:flutter/material.dart';

class AvatarView extends StatelessWidget {
  final double radius;
  final double iconSize;
  final double distance;
  final Image image;
  AvatarView({Key key, this.radius, this.iconSize, this.distance, this.image});
  @override
  Widget build(Object context) {
    return Center(
      child: Stack(
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            CircleAvatar(
              radius: radius,
              child: image,
              backgroundColor: Colors.transparent,
            ),
            Positioned(
                top: -(radius + iconSize + distance),
                right: 0,
                bottom: radius,
                left: 0,
                child: Icon(
                  Icons.access_alarm,
                  color: Colors.blue,
                  size: iconSize,
                )),
            Positioned(
                top: -(iconSize + radius),
                right: -(radius + iconSize - distance),
                bottom: iconSize,
                left: radius,
                child: Icon(
                  Icons.email,
                  color: Colors.blue,
                  size: iconSize,
                )),
            Positioned(
                top: -(radius - distance),
                right: -(radius + iconSize + distance),
                bottom: -iconSize,
                left: radius,
                child: Icon(
                  Icons.account_balance,
                  color: Colors.blue,
                  size: iconSize,
                )),
          ]),
    );
  }
}
