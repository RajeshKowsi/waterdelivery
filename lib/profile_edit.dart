import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/models/division_response.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:device_info/device_info.dart';
import 'package:water/models/township_request.dart';
import 'package:water/models/township_response.dart';
import 'package:water/myalert_dialog.dart';
import 'package:image_picker_type/image_picker_type.dart';
import 'models/login_response.dart';

class ProfileEditPage extends StatefulWidget {
  ProfileEditPage({Key key}) : super(key: key);
  @override
  ProfileEditState createState() => new ProfileEditState();
}

class ProfileEditState extends State<ProfileEditPage> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController agentCodeController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController cpasswordController = TextEditingController();

  final FocusNode firstNameFocus = FocusNode();
  final FocusNode lastNameFocus = FocusNode();
  final FocusNode mobileNumberFocus = FocusNode();
  final FocusNode agentCodeFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode cpasswordFocus = FocusNode();

  List<Division> divisionList = new List();
  List<Township> townshipList = new List();
  Division selectedDivision;
  Township selectedTownship;

  LoginResponse userDetail;

  File _image;

  int userType = 0;

  initGalleryPickUp() async {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ImagePickerHelper(
            // isSave: true,  //if you want to save image in directory
            size: Size(300, 300),
            onDone: (file) {
              if (file == null) {
                print(null);
              } else {
                setState(() {
                  _image = file;
                });
              }
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getUserDetails().then((value) {
      getDivisions().then((value) {});
    });
  }

  Future<void> getDivisions() async {
    new UserRepository().getDivision().then((divisionResponse) {
      print("divisionList ==>" + divisionResponse.toJson().toString());
      if (!divisionResponse.error) {
        divisionList = divisionResponse.data;

        selectedDivision = divisionList.firstWhere(
            (division) => division.dName == userDetail.data.division);

        if (selectedDivision != null) {
          getTownshipByDivisionId(true);
        }
      }
    });
  }

  Future<void> getTownshipByDivisionId(bool isLoadFromProfile) async {
    TownshipRequest request =
        new TownshipRequest(divisionId: selectedDivision.id);

    new UserRepository()
        .getTownshipByDivisionId(request)
        .then((townshipResponse) {
      print("Township Response =" + townshipResponse.toJson().toString());
      if (!townshipResponse.error) {
        setState(() {
          townshipList = townshipResponse.data;
        });
      } else {
        townshipList = new List();
      }
      if (isLoadFromProfile) {
        selectedTownship = townshipList.firstWhere(
            (township) => township.tName == userDetail.data.townShip);
      }
    });
  }

  Future<void> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      firstNameController.text = userDetail.data.firstName;
      lastNameController.text = userDetail.data.lastName;
      mobileNumberController.text = userDetail.data.mobileNumber;
      agentCodeController.text = userDetail.data.agentCode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: const Text('Update Profile'),
        ),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Form(
            key: _formLoader,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        print("ImagePicker Should be called");
                        initGalleryPickUp();
                      },
                      child: Container(
                          width: 100,
                          height: 100,
                          margin: EdgeInsets.only(bottom: 5, top: 10),
                          child: ClipOval(
                            child: _image == null
                                ? Image.network(userDetail.data.profileImage,
                                    fit: BoxFit.cover)
                                : Image.file(_image),
                          ))),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'First name is required field.';
                              }
                              return null;
                            },
                            focusNode: firstNameFocus,
                            controller: firstNameController,
                            keyboardType: TextInputType.text,
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "First Name",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              firstNameFocus.unfocus();
                              if (lastNameFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(lastNameFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Last name is required field.';
                              }
                              return null;
                            },
                            focusNode: lastNameFocus,
                            controller: lastNameController,
                            keyboardType: TextInputType.text,
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Last Name",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              lastNameFocus.unfocus();
                              if (mobileNumberFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(mobileNumberFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: Text(
                            "09",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Mobile number is required field.';
                              }
                              return null;
                            },
                            focusNode: mobileNumberFocus,
                            controller: mobileNumberController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Mobile Number",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              mobileNumberFocus.unfocus();
                              if (agentCodeFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(agentCodeFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  if (userDetail.data.userType == 1)
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey.withOpacity(0.5),
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      margin: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                      child: Row(
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15.0),
                            child: Icon(
                              Icons.people_outline,
                              color: Colors.grey,
                            ),
                          ),
                          Container(
                            height: 30.0,
                            width: 1.0,
                            color: Colors.grey.withOpacity(0.5),
                            margin:
                                const EdgeInsets.only(left: 00.0, right: 10.0),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 8),
                            child: Text(
                              "09",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          new Expanded(
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Agent mobile number is required field.';
                                }
                                return null;
                              },
                              focusNode: agentCodeFocus,
                              controller: agentCodeController,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              obscureText: false,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Agent Mobile Number",
                                hintStyle: TextStyle(color: Colors.grey),
                              ),
                              onFieldSubmitted: (value) {
                                agentCodeFocus.unfocus();
                                if (passwordFocus != null) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.location_city,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: DropdownButton<Division>(
                            value: selectedDivision,
                            hint: new Text("Select your division"),
                            isExpanded: true,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 45,
                            underline: Container(
                              height: 0,
                            ),
                            onChanged: (Division division) {
                              if (selectedDivision != division) {
                                setState(() {
                                  selectedTownship = null;
                                  selectedDivision = division;
                                  getTownshipByDivisionId(false);
                                });
                              }
                            },
                            items: divisionList.map<DropdownMenuItem<Division>>(
                                (Division division) {
                              return DropdownMenuItem<Division>(
                                value: division,
                                child: Container(
                                  child: new Text(division.dName),
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.location_city,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: DropdownButton<Township>(
                            value: selectedTownship,
                            hint: new Text("Select your township"),
                            isExpanded: true,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 45,
                            underline: Container(
                              height: 0,
                            ),
                            onChanged: (Township township) {
                              setState(() {
                                selectedTownship = township;
                              });
                            },
                            items: townshipList.map<DropdownMenuItem<Township>>(
                                (Township township) {
                              return DropdownMenuItem<Township>(
                                value: township,
                                child: Container(
                                  child: new Text(township.tName),
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: FlatButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            splashColor: Color(0xFF4aa0d5),
                            color: Color(0xFF4aa0d5),
                            child: new Row(
                              children: <Widget>[
                                new Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    "Update",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                                new Expanded(
                                  child: Container(),
                                ),
                                new Transform.translate(
                                  offset: Offset(15.0, 0.0),
                                  child: new Container(
                                    padding: const EdgeInsets.all(5.0),
                                    child: FlatButton(
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(28.0)),
                                      splashColor: Colors.white,
                                      color: Colors.white,
                                      child: Icon(
                                        Icons.arrow_forward,
                                        color: Color(0xFF4aa0d5),
                                      ),
                                      onPressed: () {
                                        updateUserDetails();
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onPressed: () {
                              updateUserDetails();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
        ));
  }

  updateUserDetails() {
    if (!_formLoader.currentState.validate()) {
      return;
    }
    if (selectedDivision == null || selectedTownship == null) {
      MyAlertDialog.showMyDialog(context, "Registration Failed",
          "Please select your division and township...", null);
      return;
    }

    if (_image != null) {
      new UserRepository().uploadImage(_image.path).then((response) {
        print("Image Uploaded url ==>" + response.data);
        updateUserProfile(response.data);
      });
    } else {
      updateUserProfile("");
    }
  }

  updateUserProfile(String profilePicUrl) {
    Navigator.pop(context);
  }
}
