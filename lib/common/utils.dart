import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Utils {
  static bool isLoadingIndicatorShown = false;
  static bool isProfileImageUpdated = false;
  // ignore: missing_return
  static Future<bool> onBackPressed(
      BuildContext context, final GlobalKey<ScaffoldState> _scaffoldKey) {
    bool isDrawerOpen = _scaffoldKey.currentState.isDrawerOpen;
    if (isDrawerOpen) {
      Navigator.of(context).pop();
      return showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              title: new Text('Are you sure?'),
              content: new Text('Do you want to exit an App'),
              actions: <Widget>[
                FlatButton(
                  child: Text('NO'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                FlatButton(
                  child: Text('YES'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            ),
          ) ??
          false;
    } else {
      _scaffoldKey.currentState.openDrawer();
    }
  }

  static void hideLoadingDialog(BuildContext context) {
    if (isLoadingIndicatorShown) {
      isLoadingIndicatorShown = false;
      Navigator.pop(context);
    }
  }

  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    isLoadingIndicatorShown = true;
    Future.delayed(const Duration(milliseconds: 5000), () {
      hideLoadingDialog(context);
    });
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 80,
                        height: 50,
                        child: Column(
                            children: [SpinKitCircle(color: Colors.blue)]),
                      ),
                    )
                  ]));
        });
  }
}
