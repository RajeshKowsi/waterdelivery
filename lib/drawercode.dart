import 'package:flutter/material.dart';
import 'package:water/models/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerCode extends StatefulWidget {
  final LoginResponse userDetail;
  DrawerCode({Key key, this.userDetail}) : super(key: key);

  @override
  _DrawerCodeState createState() => _DrawerCodeState();
}

class _DrawerCodeState extends State<DrawerCode> {
  //LoginResponse userDetail;
  String userName = "";
  String profileImageUrl = "";
  String userType = "";
  var _loadImage = new AssetImage("graphics/user_profile.png");
  var _myprofileImage;

  bool _checkLoaded = true;

  // Future<void> getUserDetails() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String userString = prefs.getString("userdetails");
  //   print("####### user details ===>" + userString);
  //   userDetail = LoginResponse.fromJson(jsonDecode(userString));
  //   print("####### LoginResponse ===>" + userDetail.toJson().toString());
  //   userName = userDetail.data.firstName + " " + userDetail.data.lastName;
  //   profileImageUrl = userDetail.data.profileImage;
  //   _myprofileImage = new NetworkImage(profileImageUrl);
  //   _myprofileImage
  //       .resolve(new ImageConfiguration())
  //       .addListener(ImageStreamListener((info, call) {
  //     if (mounted) {
  //       setState(() {
  //         _checkLoaded = false;
  //       });
  //     }
  //   }));
  // }

  Future<void> clearUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userdetails", null);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    userName = widget.userDetail.data.firstName +
        " " +
        widget.userDetail.data.lastName;

    profileImageUrl = widget.userDetail.data.profileImage;

    userType = (widget.userDetail.data.userType == 1) ? "Customer" : "Agent";

    _myprofileImage = new NetworkImage(profileImageUrl);
    _myprofileImage
        .resolve(new ImageConfiguration())
        .addListener(ImageStreamListener((info, call) {
      if (mounted) {
        setState(() {
          _checkLoaded = false;
        });
      }
    }));

    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 10),
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          margin: EdgeInsets.only(top: 16),
                          width: 30,
                          height: 30,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.black)),
                          child: CircleAvatar(
                            radius: 15.0,
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.close,
                              color: Colors.black,
                              size: 30,
                            ),
                          ),
                        )),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 100,
                        height: 100,
                        margin: EdgeInsets.only(top: 0, bottom: 5),
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.blue)),
                        child: Padding(
                          padding: EdgeInsets.all(6.0),
                          child: new CircleAvatar(
                            backgroundColor:
                                Theme.of(context).scaffoldBackgroundColor,
                            backgroundImage:
                                _checkLoaded ? _loadImage : _myprofileImage,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      child: Text(
                        userName,
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      child: Text(
                        userType,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  const Divider(
                    color: Colors.black,
                    height: 20,
                    thickness: 3,
                    indent: 0,
                    endIndent: 150,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Text(
                "Home",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, '/home');
              },
            ),
            ListTile(
              title: Text(
                "About",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/profile');
              },
            ),
            if (widget.userDetail != null &&
                widget.userDetail.data.userType == 1)
              ListTile(
                title: Text(
                  "Booking",
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/booking');
                },
              ),
            ListTile(
              title: Text(
                "Cart",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/myorders');
                print("Go to My Orders");
              },
            ),
            SizedBox(height: 20.0),
            const Divider(
              color: Colors.black,
              height: 20,
              thickness: 3,
              indent: 15,
              endIndent: 165,
            ),
            ListTile(
              title: Text(
                "Sign out",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              onTap: () {
                print("Logout Users");
                clearUserDetails().then((value) {
                  Navigator.pop(context);
                  Navigator.pushReplacementNamed(context, '/login');
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
