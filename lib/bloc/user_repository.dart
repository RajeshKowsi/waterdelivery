import 'package:water/models/agentmap_request.dart';
import 'package:water/models/agentmap_response.dart';
import 'package:water/models/brand_request.dart';
import 'package:water/models/brand_response.dart';
import 'package:water/models/division_response.dart';
import 'package:water/models/lift_api_request.dart';
import 'package:water/models/lift_api_response.dart';
import 'package:water/models/login_request.dart';
import 'package:water/models/login_response.dart';
import 'package:water/models/myorder_request.dart';
import 'package:water/models/myorder_response.dart';
import 'package:water/models/order_request.dart';
import 'package:water/models/order_response.dart';
import 'package:water/models/send_sms_agent.dart';
import 'package:water/models/send_sms_response.dart';
import 'package:water/models/township_request.dart';
import 'package:water/models/township_response.dart';
import 'package:water/models/update_fcm_request.dart';
import 'package:water/models/update_fcm_response.dart';
import 'package:water/models/update_order_request.dart';
import 'package:water/models/update_order_response.dart';
import 'package:water/models/update_password_request.dart';
import 'package:water/models/update_password_response.dart';
import 'package:water/models/update_user_request.dart';
import 'package:water/models/update_user_response.dart';
import 'package:water/models/upload_file_response.dart';
import 'package:water/models/user_exists_request.dart';
import 'package:water/models/user_exists_response.dart';
import 'package:water/networking/api_provider.dart';
import 'dart:async';
import 'package:water/models/signup_request.dart';
import 'package:water/models/signup_response.dart';
import 'package:water/models/nearbyagent_request.dart';
import 'package:water/models/nearbyagent_response.dart';

class UserRepository {
  ApiProvider _provider = ApiProvider();

  Future<SignupResponse> registerUser(SignupRequest data) async {
    final response = await _provider.post("agentRegister_w", data);
    return SignupResponse.fromJson(response);
  }

  Future<LoginResponse> loginUser(LoginRequest data) async {
    final response = await _provider.post("login_w", data);
    return LoginResponse.fromJson(response);
  }

  Future<NearByAgentResponse> getNearByAgent(NearByAgentRequest data) async {
    final response = await _provider.post("nearbyAgent", data);
    return NearByAgentResponse.fromJson(response);
  }

  Future<OrderResponse> placeOrder(OrderRequest data) async {
    final response = await _provider.post("OrderPlace", data);
    return OrderResponse.fromJson(response);
  }

  Future<AgentMapReponse> mapUserAgent(AgentMapRequest data) async {
    final response = await _provider.post("userAgentMap", data);
    return AgentMapReponse.fromJson(response);
  }

  Future<BrandResponse> getBrandList(BrandRequest data) async {
    final response = await _provider.post("getBrandData", data);
    return BrandResponse.fromJson(response);
  }

  Future<MyOrderResponse> getMyOrderHistory(MyOrderRequest data) async {
    final response = await _provider.post("getMyOrder", data);
    return MyOrderResponse.fromJson(response);
  }

  Future<UpdateOrderResponse> updateOrder(UpdateOrderRequest data) async {
    final response = await _provider.post("UpdateOrder", data);
    return UpdateOrderResponse.fromJson(response);
  }

  Future<UpdateUserResponse> updateUser(UpdateUserRequest data) async {
    final response = await _provider.post("updateUser", data);
    return UpdateUserResponse.fromJson(response);
  }

  Future<UpdateFCMResponse> updateFCMToken(UpdateFCMRequest data) async {
    final response = await _provider.post("updateFCMID", data);
    return UpdateFCMResponse.fromJson(response);
  }

  Future<UploadFileResponse> uploadImage(String filePath) async {
    final response = await _provider.uploadImage("FileUpload", filePath);
    return UploadFileResponse.fromJson(response);
  }

  Future<DivisionResponse> getDivision() async {
    final response = await _provider.get("Division");
    return DivisionResponse.fromJson(response);
  }

  Future<TownshipResponse> getTownshipByDivisionId(TownshipRequest data) async {
    final response = await _provider.post("Township", data);
    return TownshipResponse.fromJson(response);
  }

  Future<LiftAPIResponse> getLiftNoData(LiftAPIRequest data) async {
    final response = await _provider.post("LiftDataNo", data);
    return LiftAPIResponse.fromJson(response);
  }

  Future<LiftAPIResponse> getLiftYesData(LiftAPIRequest data) async {
    final response = await _provider.post("LiftData", data);
    return LiftAPIResponse.fromJson(response);
  }

  Future<SendSMSResponse> sendSMS(SendSMSRequest data) async {
    final response = await _provider.post(
        "http://advertisement.api.okdollar.org/AdService.svc/SendSmsGeneric",
        data);
    return SendSMSResponse.fromJson(response);
  }

  Future<UserExistsResponse> checkUserExists(UserExistsRequest data) async {
    final response = await _provider.post("UserExist", data);
    return UserExistsResponse.fromJson(response);
  }

  Future<UpdatePasswordResponse> updatePassword(
      UpdatePasswordRequest data) async {
    final response = await _provider.post("updatePassword", data);
    return UpdatePasswordResponse.fromJson(response);
  }
}
