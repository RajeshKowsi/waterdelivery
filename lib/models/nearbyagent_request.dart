class NearByAgentRequest {
  String mobileNumber;
  String latitude;
  String longitude;
  String agentType;

  NearByAgentRequest(
      {this.mobileNumber, this.latitude, this.longitude, this.agentType});

  NearByAgentRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    agentType = json['AgentType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['AgentType'] = this.agentType;
    return data;
  }
}
