class UpdateFCMRequest {
  String userId;
  String userType;
  String fCMID;

  UpdateFCMRequest({this.userId, this.userType, this.fCMID});

  UpdateFCMRequest.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    userType = json['UserType'];
    fCMID = json['FCMID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserType'] = this.userType;
    data['FCMID'] = this.fCMID;
    return data;
  }
}
