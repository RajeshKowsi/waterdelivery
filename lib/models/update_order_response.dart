class UpdateOrderResponse {
  int code;
  bool error;
  String msg;
  List<int> data;

  UpdateOrderResponse({this.code, this.error, this.msg, this.data});

  UpdateOrderResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    data['data'] = this.data;
    return data;
  }
}
