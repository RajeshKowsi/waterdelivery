class UpdateUserRequest {
  String mobileNumber;
  String latitude;
  String longitude;
  String address;
  String profilePicture;
  String township;
  String division;
  String street;
  String userType;

  UpdateUserRequest(
      {this.mobileNumber,
      this.latitude,
      this.longitude,
      this.address,
      this.profilePicture,
      this.township,
      this.division,
      this.street,
      this.userType});

  UpdateUserRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    address = json['Address'];
    profilePicture = json['ProfilePicture'];
    township = json['Township'];
    division = json['Division'];
    street = json['Street'];
    userType = json['UserType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['Address'] = this.address;
    data['ProfilePicture'] = this.profilePicture;
    data['Township'] = this.township;
    data['Division'] = this.division;
    data['Street'] = this.street;
    data['UserType'] = this.userType;
    return data;
  }
}
