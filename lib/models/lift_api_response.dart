class LiftAPIResponse {
  int code;
  bool error;
  String msg;
  List<LiftData> data;

  LiftAPIResponse({this.code, this.error, this.msg, this.data});

  LiftAPIResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<LiftData>();
      json['data'].forEach((v) {
        data.add(new LiftData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LiftData {
  String id;
  String floorNo;
  int floorPrice;
  bool active;
  String createdAt;
  String updatedAt;

  LiftData(
      {this.id,
      this.floorNo,
      this.floorPrice,
      this.active,
      this.createdAt,
      this.updatedAt});

  LiftData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    floorNo = json['floorNo'];
    floorPrice = json['floorPrice'];
    active = json['active'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['floorNo'] = this.floorNo;
    data['floorPrice'] = this.floorPrice;
    data['active'] = this.active;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
