class DivisionResponse {
  int code;
  bool error;
  String msg;
  List<Division> data;

  DivisionResponse({this.code, this.error, this.msg, this.data});

  DivisionResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<Division>();
      json['data'].forEach((v) {
        data.add(new Division.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Division {
  String id;
  String dName;
  bool active;
  String createdAt;
  String updatedAt;

  Division({this.id, this.dName, this.active, this.createdAt, this.updatedAt});

  Division.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    dName = json['dName'];
    active = json['active'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['dName'] = this.dName;
    data['active'] = this.active;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
