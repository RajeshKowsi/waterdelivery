class AgentMapRequest {
  String mobileNumber;
  String userId;
  String agentId;
  String agentMobileNumber;

  AgentMapRequest(
      {this.mobileNumber, this.userId, this.agentId, this.agentMobileNumber});

  AgentMapRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    userId = json['UserId'];
    agentId = json['AgentId'];
    agentMobileNumber = json['AgentMobileNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['UserId'] = this.userId;
    data['AgentId'] = this.agentId;
    data['AgentMobileNumber'] = this.agentMobileNumber;
    return data;
  }
}
