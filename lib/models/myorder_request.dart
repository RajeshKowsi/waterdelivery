class MyOrderRequest {
  String userId;
  String userType;

  MyOrderRequest({this.userId, this.userType});

  MyOrderRequest.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    userType = json['UserType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserType'] = this.userType;
    return data;
  }
}
