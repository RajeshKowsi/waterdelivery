class BrandResponse {
  int code;
  bool error;
  String msg;
  List<Brand> data;

  BrandResponse({this.code, this.error, this.msg, this.data});

  BrandResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<Brand>();
      json['data'].forEach((v) {
        data.add(new Brand.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Brand {
  String id;
  String imageUrl;
  String brandName;
  String brandDescription;
  int amt;
  String createdAt;
  String updatedAt;

  Brand(
      {this.id,
      this.imageUrl,
      this.brandName,
      this.brandDescription,
      this.amt,
      this.createdAt,
      this.updatedAt});

  Brand.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    imageUrl = json['imageUrl'];
    brandName = json['brandName'];
    brandDescription = json['brandDescription'];
    amt = json['Amt'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['imageUrl'] = this.imageUrl;
    data['brandName'] = this.brandName;
    data['brandDescription'] = this.brandDescription;
    data['Amt'] = this.amt;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
