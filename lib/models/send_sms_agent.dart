class SendSMSRequest {
  String mobileNumber;
  Null packageName;
  String smsContent;

  SendSMSRequest({this.mobileNumber, this.packageName, this.smsContent});

  SendSMSRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    packageName = json['PackageName'];
    smsContent = json['SmsContent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['PackageName'] = this.packageName;
    data['SmsContent'] = this.smsContent;
    return data;
  }
}
