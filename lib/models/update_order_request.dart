class UpdateOrderRequest {
  String orderId;
  bool orderStatus;

  UpdateOrderRequest({this.orderId, this.orderStatus});

  UpdateOrderRequest.fromJson(Map<String, dynamic> json) {
    orderId = json['OrderId'];
    orderStatus = json['OrderStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OrderId'] = this.orderId;
    data['OrderStatus'] = this.orderStatus;
    return data;
  }
}
