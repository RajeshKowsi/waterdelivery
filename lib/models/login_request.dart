class LoginRequest {
  String mobileNumber;
  String password;

  LoginRequest({this.mobileNumber, this.password});

  LoginRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    password = json['Password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Password'] = this.password;
    return data;
  }
}
