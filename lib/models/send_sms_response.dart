class SendSMSResponse {
  int code;
  String data;
  String msg;

  SendSMSResponse({this.code, this.data, this.msg});

  SendSMSResponse.fromJson(Map<String, dynamic> json) {
    code = json['Code'];
    data = json['Data'];
    msg = json['Msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Code'] = this.code;
    data['Data'] = this.data;
    data['Msg'] = this.msg;
    return data;
  }
}
