class TownshipResponse {
  int code;
  bool error;
  String msg;
  List<Township> data;

  TownshipResponse({this.code, this.error, this.msg, this.data});

  TownshipResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<Township>();
      json['data'].forEach((v) {
        data.add(new Township.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Township {
  String id;
  String tName;
  String divisionid;
  bool active;
  String createdAt;
  String updatedAt;

  Township(
      {this.id,
      this.tName,
      this.divisionid,
      this.active,
      this.createdAt,
      this.updatedAt});

  Township.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    tName = json['TName'];
    divisionid = json['divisionid'];
    active = json['active'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['TName'] = this.tName;
    data['divisionid'] = this.divisionid;
    data['active'] = this.active;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
