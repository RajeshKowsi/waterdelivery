class UpdatePasswordRequest {
  String mobileNumber;
  String password;
  String userType;

  UpdatePasswordRequest({this.mobileNumber, this.password, this.userType});

  UpdatePasswordRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    password = json['Password'];
    userType = json['UserType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Password'] = this.password;
    data['UserType'] = this.userType;
    return data;
  }
}
