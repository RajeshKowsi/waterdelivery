class UpdateUserResponse {
  int code;
  bool error;
  String msg;
  Data data;

  UpdateUserResponse({this.code, this.error, this.msg, this.data});

  UpdateUserResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String mobileNumber;
  String address;
  String profilePicture;
  String township;
  String division;
  String street;
  String userType;

  Data(
      {this.mobileNumber,
      this.address,
      this.profilePicture,
      this.township,
      this.division,
      this.street,
      this.userType});

  Data.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    address = json['Address'];
    profilePicture = json['ProfilePicture'];
    township = json['Township'];
    division = json['Division'];
    street = json['Street'];
    userType = json['UserType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Address'] = this.address;
    data['ProfilePicture'] = this.profilePicture;
    data['Township'] = this.township;
    data['Division'] = this.division;
    data['Street'] = this.street;
    data['UserType'] = this.userType;
    return data;
  }
}
