class BrandRequest {
  String mobileNumber;

  BrandRequest({this.mobileNumber});

  BrandRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    return data;
  }
}
