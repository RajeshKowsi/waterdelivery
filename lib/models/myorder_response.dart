class MyOrderResponse {
  int code;
  bool error;
  String msg;
  List<MyOrder> data;

  MyOrderResponse({this.code, this.error, this.msg, this.data});

  MyOrderResponse.fromJson(Map<String, dynamic> json) {
    code = (json['code'] is String) ? int.parse(json['code']) : json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<MyOrder>();
      json['data'].forEach((v) {
        data.add(new MyOrder.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MyOrder {
  String id;
  String userpId;
  String agentId;
  int amount;
  int points;
  int discountAmt;
  int count;
  String brandName;
  String brandId;
  bool deliveryStatus;
  String latitude;
  String longitude;
  String address;
  String township;
  String street;
  String division;
  bool locationType;
  String createdAt;
  String updatedAt;
  String userPId;
  OrderAgent agent;

  MyOrder(
      {this.id,
      this.userpId,
      this.agentId,
      this.amount,
      this.points,
      this.discountAmt,
      this.count,
      this.brandName,
      this.brandId,
      this.deliveryStatus,
      this.latitude,
      this.longitude,
      this.address,
      this.township,
      this.street,
      this.division,
      this.locationType,
      this.createdAt,
      this.updatedAt,
      this.userPId,
      this.agent});

  MyOrder.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userpId = json['userpId'];
    agentId = json['agentId'];
    amount = json['amount'];
    points = json['points'];
    discountAmt = json['discountAmt'];
    count = json['count'];
    brandName = json['brandName'];
    brandId = json['brandId'];
    deliveryStatus = json['deliveryStatus'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    township = json['township'];
    street = json['street'];
    division = json['division'];
    locationType = json['locationType'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    userPId = json['userPId'];
    agent = json['agent'] != null
        ? new OrderAgent.fromJson(json['agent'])
        : json['userP'] != null
            ? new OrderAgent.fromJson(json['userP'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['userpId'] = this.userpId;
    data['agentId'] = this.agentId;
    data['amount'] = this.amount;
    data['points'] = this.points;
    data['discountAmt'] = this.discountAmt;
    data['count'] = this.count;
    data['brandName'] = this.brandName;
    data['brandId'] = this.brandId;
    data['deliveryStatus'] = this.deliveryStatus;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['address'] = this.address;
    data['township'] = this.township;
    data['street'] = this.street;
    data['division'] = this.division;
    data['locationType'] = this.locationType;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['userPId'] = this.userPId;
    if (this.agent != null) {
      data['agent'] = this.agent.toJson();
    }
    return data;
  }
}

class OrderAgent {
  String id;
  String firstname;
  String lastname;
  String mobileno;
  String password;
  String profilepic;
  String address;
  String township;
  String street;
  String division;
  String latitude;
  String longitude;
  int usertype;
  bool isactive;
  String createdAt;
  String updatedAt;

  OrderAgent(
      {this.id,
      this.firstname,
      this.lastname,
      this.mobileno,
      this.password,
      this.profilepic,
      this.address,
      this.township,
      this.street,
      this.division,
      this.latitude,
      this.longitude,
      this.usertype,
      this.isactive,
      this.createdAt,
      this.updatedAt});

  OrderAgent.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    mobileno = json['mobileno'];
    password = json['password'];
    profilepic = json['profilepic'];
    address = json['address'];
    township = json['township'];
    street = json['street'];
    division = json['division'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    usertype = json['usertype'];
    isactive = json['isactive'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['mobileno'] = this.mobileno;
    data['password'] = this.password;
    data['profilepic'] = this.profilepic;
    data['address'] = this.address;
    data['township'] = this.township;
    data['street'] = this.street;
    data['division'] = this.division;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['usertype'] = this.usertype;
    data['isactive'] = this.isactive;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
