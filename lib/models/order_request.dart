class OrderRequest {
  String mobileNumber;
  String userId;
  String agentId;
  String agentMobileNumber;
  String amount;
  String count;
  String brandName;
  String brandId;
  String locationtype;
  String latitude;
  String longitude;
  String address;
  String township;
  String division;
  String street;
  String otherNumber;
  String floorNo;

  OrderRequest(
      {this.mobileNumber,
      this.userId,
      this.agentId,
      this.agentMobileNumber,
      this.amount,
      this.count,
      this.brandName,
      this.brandId,
      this.locationtype,
      this.latitude,
      this.longitude,
      this.address,
      this.township,
      this.division,
      this.street,
      this.otherNumber,
      this.floorNo});

  OrderRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    userId = json['UserId'];
    agentId = json['AgentId'];
    agentMobileNumber = json['AgentMobileNumber'];
    amount = json['Amount'];
    count = json['Count'];
    brandName = json['BrandName'];
    brandId = json['BrandId'];
    locationtype = json['Locationtype'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    address = json['Address'];
    township = json['TownShip'];
    division = json['Division'];
    street = json['Street'];
    otherNumber = json['OtherNumber'];
    floorNo = json['FloorNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['UserId'] = this.userId;
    data['AgentId'] = this.agentId;
    data['AgentMobileNumber'] = this.agentMobileNumber;
    data['Amount'] = this.amount;
    data['Count'] = this.count;
    data['BrandName'] = this.brandName;
    data['BrandId'] = this.brandId;
    data['Locationtype'] = this.locationtype;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['Address'] = this.address;
    data['TownShip'] = this.township;
    data['Division'] = this.division;
    data['Street'] = this.street;
    data['OtherNumber'] = this.otherNumber;
    data['FloorNo'] = this.floorNo;
    return data;
  }
}
