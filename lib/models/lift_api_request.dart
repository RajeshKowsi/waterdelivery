class LiftAPIRequest {
  String mno;

  LiftAPIRequest({this.mno});

  LiftAPIRequest.fromJson(Map<String, dynamic> json) {
    mno = json['Mno'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Mno'] = this.mno;
    return data;
  }
}
