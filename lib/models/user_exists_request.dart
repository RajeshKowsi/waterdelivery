class UserExistsRequest {
  String mobileNumber;
  String smsContent;

  UserExistsRequest({this.mobileNumber, this.smsContent});

  UserExistsRequest.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    smsContent = json['SmsContent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['SmsContent'] = this.smsContent;
    return data;
  }
}
