class SignupRequest {
  String firstName;
  String lastName;
  String mobileNumber;
  String agentCode;
  String deviceModel;
  String deviceType;
  String profileImage;
  String deviceMake;
  String deviceUniqueId;
  String password;
  String userType;
  String isActive;
  String division;
  String township;
  String latitude;
  String longitude;

  SignupRequest(
      {this.firstName,
      this.lastName,
      this.mobileNumber,
      this.agentCode,
      this.deviceModel,
      this.deviceType,
      this.profileImage,
      this.deviceMake,
      this.deviceUniqueId,
      this.password,
      this.userType,
      this.isActive,
      this.division,
      this.township,
      this.latitude,
      this.longitude});

  SignupRequest.fromJson(Map<String, dynamic> json) {
    firstName = json['FirstName'];
    lastName = json['LastName'];
    mobileNumber = json['MobileNumber'];
    agentCode = json['AgentCode'];
    deviceModel = json['DeviceModel'];
    deviceType = json['DeviceType'];
    profileImage = json['ProfileImage'];
    deviceMake = json['DeviceMake'];
    deviceUniqueId = json['DeviceUniqueId'];
    password = json['Password'];
    userType = json['UserType'];
    isActive = json['IsActive'];
    division = json['Division'];
    township = json['TownShip'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['MobileNumber'] = this.mobileNumber;
    data['AgentCode'] = this.agentCode;
    data['DeviceModel'] = this.deviceModel;
    data['DeviceType'] = this.deviceType;
    data['ProfileImage'] = this.profileImage;
    data['DeviceMake'] = this.deviceMake;
    data['DeviceUniqueId'] = this.deviceUniqueId;
    data['Password'] = this.password;
    data['UserType'] = this.userType;
    data['IsActive'] = this.isActive;
    data['Division'] = this.division;
    data['TownShip'] = this.township;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    return data;
  }
}
