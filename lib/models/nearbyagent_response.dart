class NearByAgentResponse {
  int code;
  bool error;
  String msg;
  List<Agent> data;

  NearByAgentResponse({this.code, this.error, this.msg, this.data});

  NearByAgentResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = new List<Agent>();
      json['data'].forEach((v) {
        data.add(new Agent.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Agent {
  String id;
  String mobileno;
  String firstname;
  String lastname;
  String agentCode;
  String profilepic;
  String latitude;
  String longitude;
  double distance;

  Agent(
      {this.id,
      this.mobileno,
      this.firstname,
      this.lastname,
      this.agentCode,
      this.profilepic,
      this.latitude,
      this.longitude,
      this.distance});

  Agent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mobileno = json['mobileno'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    agentCode = json['agentCode'];
    profilepic = json['profilepic'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mobileno'] = this.mobileno;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['agentCode'] = this.agentCode;
    data['profilepic'] = this.profilepic;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    return data;
  }
}
