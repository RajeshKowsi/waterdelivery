class OrderResponse {
  int code;
  bool error;
  String msg;
  Data data;

  OrderResponse({this.code, this.error, this.msg, this.data});

  OrderResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String id;
  String userId;
  String agentId;
  String amount;
  String points;
  String discountAmt;
  String count;
  bool deliveryStatus;
  String brandName;
  String brandId;
  String updatedAt;
  String createdAt;

  Data(
      {this.id,
      this.userId,
      this.agentId,
      this.amount,
      this.points,
      this.discountAmt,
      this.count,
      this.deliveryStatus,
      this.brandName,
      this.brandId,
      this.updatedAt,
      this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['userId'];
    agentId = json['agentId'];
    amount = json['amount'];
    points = json['points'];
    discountAmt = json['discountAmt'];
    count = json['count'];
    deliveryStatus = json['deliveryStatus'];
    brandName = json['brandName'];
    brandId = json['brandId'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['userId'] = this.userId;
    data['agentId'] = this.agentId;
    data['amount'] = this.amount;
    data['points'] = this.points;
    data['discountAmt'] = this.discountAmt;
    data['count'] = this.count;
    data['deliveryStatus'] = this.deliveryStatus;
    data['brandName'] = this.brandName;
    data['brandId'] = this.brandId;
    data['updatedAt'] = this.updatedAt;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
