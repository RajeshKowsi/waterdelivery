class UpdateFCMResponse {
  int code;
  bool error;
  String msg;
  Data data;

  UpdateFCMResponse({this.code, this.error, this.msg, this.data});

  UpdateFCMResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String id;
  String agentId;
  String fcmid;
  String usertype;
  bool isactive;
  String updatedAt;
  String createdAt;

  Data(
      {this.id,
      this.agentId,
      this.fcmid,
      this.usertype,
      this.isactive,
      this.updatedAt,
      this.createdAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    agentId = json['agentId'];
    fcmid = json['fcmid'];
    usertype = json['usertype'];
    isactive = json['isactive'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['agentId'] = this.agentId;
    data['fcmid'] = this.fcmid;
    data['usertype'] = this.usertype;
    data['isactive'] = this.isactive;
    data['updatedAt'] = this.updatedAt;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
