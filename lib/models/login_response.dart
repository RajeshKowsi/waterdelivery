class LoginResponse {
  int code;
  bool error;
  String msg;
  Data data;

  LoginResponse({this.code, this.error, this.msg, this.data});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String userId;
  String firstName;
  String lastName;
  String mobileNumber;
  String profileImage;
  int userType;
  String agentCode;
  bool isActive;
  String latitude;
  String longitude;
  String address;
  String townShip;
  String street;
  String division;
  AgentDetails agentDetails;

  Data(
      {this.userId,
      this.firstName,
      this.lastName,
      this.mobileNumber,
      this.profileImage,
      this.userType,
      this.agentCode,
      this.isActive,
      this.latitude,
      this.longitude,
      this.address,
      this.townShip,
      this.street,
      this.division,
      this.agentDetails});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    firstName = json['firstName'];
    lastName = json['LastName'];
    mobileNumber = json['MobileNumber'];
    profileImage = json['ProfileImage'];
    userType = json['UserType'];
    agentCode = json['AgentCode'];
    isActive = json['IsActive'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    address = json['Address'];
    townShip = json['TownShip'];
    street = json['Street'];
    division = json['Division'];
    agentDetails = json['AgentDetails'] != null
        ? new AgentDetails.fromJson(json['AgentDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['firstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['MobileNumber'] = this.mobileNumber;
    data['ProfileImage'] = this.profileImage;
    data['UserType'] = this.userType;
    data['AgentCode'] = this.agentCode;
    data['IsActive'] = this.isActive;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['Address'] = this.address;
    data['TownShip'] = this.townShip;
    data['Street'] = this.street;
    data['Division'] = this.division;
    if (this.agentDetails != null) {
      data['AgentDetails'] = this.agentDetails.toJson();
    }
    return data;
  }
}

class AgentDetails {
  String id;
  int userType;
  String agentId;
  String userId;

  AgentDetails({this.id, this.userType, this.agentId, this.userId});

  AgentDetails.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userType = json['UserType'];
    agentId = json['AgentId'];
    userId = json['UserId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserType'] = this.userType;
    data['AgentId'] = this.agentId;
    data['UserId'] = this.userId;
    return data;
  }
}
