class UserExistsResponse {
  int code;
  bool error;
  String msg;
  Data data;

  UserExistsResponse({this.code, this.error, this.msg, this.data});

  UserExistsResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String userId;
  String firstName;
  String lastName;
  String mobileNumber;
  String profileImage;
  int userType;

  Data(
      {this.userId,
      this.firstName,
      this.lastName,
      this.mobileNumber,
      this.profileImage,
      this.userType});

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    firstName = json['firstName'];
    lastName = json['LastName'];
    mobileNumber = json['MobileNumber'];
    profileImage = json['ProfileImage'];
    userType = json['UserType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['firstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['MobileNumber'] = this.mobileNumber;
    data['ProfileImage'] = this.profileImage;
    data['UserType'] = this.userType;
    return data;
  }
}
