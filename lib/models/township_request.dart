class TownshipRequest {
  String divisionId;

  TownshipRequest({this.divisionId});

  TownshipRequest.fromJson(Map<String, dynamic> json) {
    divisionId = json['DivisionId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DivisionId'] = this.divisionId;
    return data;
  }
}
