import 'package:flutter/material.dart';
import 'package:water/guest_home.dart';
import 'package:water/myorder_direction.dart';
import 'package:water/myorders_new.dart';
import 'package:water/onboard_page.dart';
import 'package:water/profile_edit_new.dart';
import 'package:water/register_step1.dart';
import 'package:water/register_step2.dart';
import 'package:water/reset_password.dart';
import 'package:water/myhomepage.dart';
import 'package:water/profile.dart';
import 'package:water/myagent.dart';
import 'package:water/signup_new.dart';
import 'package:water/splash_screen.dart';

import 'package:water/booking_page_new.dart';
import 'package:water/login_new_page.dart';

void main() {
  runApp(MaterialApp(
    title: 'Water App',
    theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity),
    initialRoute: '/',
    routes: {
      '/': (context) => SplashScreen(),
      '/guesthomepage': (context) => GuestHomePage(),
      '/login': (context) => LoginNewPage(),
      '/forgotpwd': (context) => ResetPasswordPage(),
      '/home': (context) => MyHomePage(),
      '/register': (context) => RegisterStepOnePage(),
      '/registerotp': (context) => RegisterStepTwoPage(),
      '/register_details': (context) => SignupNewPage(),
      '/profile': (context) => ProfilePage(),
      '/editprofile': (context) => ProfileEditPage(),
      '/myorders': (context) => MyOrdersNewPage(),
      '/myagent': (context) => MyAgentPage(),
      '/booking': (context) => BookingNewPage(),
      '/myorderdirection': (context) => MyOrderDirection(),
      '/onboardpage': (context) => OnBoardingPage()
    },
  ));
}
