import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/common/utils.dart';
import 'package:water/register_step1.dart';
import 'package:water/views/login_view.dart';
import 'bloc/user_repository.dart';
import 'models/login_request.dart';
import 'myalert_dialog.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<State> _state = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final LoginParam loginParam = ModalRoute.of(context).settings.arguments;
    String mobileNumber;
    if (loginParam != null) {
      print("LoginParam ==>" + loginParam.toString());
      mobileNumber = loginParam.mobileNumber;
    }

    return Scaffold(
      key: _state,
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
          child: Container(
        child: LoginView(
            primaryColor: Color(0xFF4aa0d5),
            backgroundColor: Colors.white,
            backgroundImage: new AssetImage("assets/images/full-bloom.png"),
            context: context,
            mobileNumber: mobileNumber,
            onSubmitCallback: onSubmitCallback),
      )),
    );
  }

  onSubmitCallback(String mobileNumber, String password) {
    Utils.showLoadingDialog(context, _state);
    LoginRequest loginRequest =
        new LoginRequest(mobileNumber: mobileNumber, password: password);
    new UserRepository().loginUser(loginRequest).then((loginResponse) {
      Utils.hideLoadingDialog(context);
      if (!loginResponse.error) {
        saveUserDetails(jsonEncode(loginResponse.toJson()));
        //Navigator.pushReplacementNamed(context, '/home');\
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
      } else {
        MyAlertDialog.showMyDialog(
            context, "Login Failed", loginResponse.msg, null);
      }
    });
  }

  Future<void> saveUserDetails(String userData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userdetails", userData);
  }
}
