import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:water/bloc/user_repository.dart';

import 'package:water/common/utils.dart';
import 'package:water/models/update_password_request.dart';
import 'package:water/myalert_dialog.dart';
import 'package:water/register_step1.dart';
import 'package:water/styles/MyAppStyles.dart';

class ResetPasswordPage extends StatefulWidget {
  ResetPasswordPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final GlobalKey<State> _state = new GlobalKey<State>();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _cpasswordFocus = FocusNode();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController cpasswordController = TextEditingController();
  bool _showPassword = true;
  bool _showcPassword = true;
  String mobileNumber;

  int userType = 0;

  int rememberMe = 0;
  @override
  Widget build(BuildContext context) {
    final OtpPageParam otpParam = ModalRoute.of(context).settings.arguments;

    if (otpParam != null) {
      print("OTPParam ==>" + otpParam.userType.toString());
      mobileNumber = otpParam.mobileNumber;
      userType = otpParam.userType;
    }

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: null,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.west_sharp,
            color: Colors.grey,
            size: 30,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Form(
        key: _formLoader,
        child: Padding(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.

            child: Column(
              // Column is also a layout widget. It takes a list of children and
              // arranges them vertically. By default, it sizes itself to fit its
              // children horizontally, and tries to be as tall as its parent.
              //
              // Invoke "debug painting" (press "p" in the console, choose the
              // "Toggle Debug Paint" action from the Flutter Inspector in Android
              // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
              // to see the wireframe for each widget.
              //
              // Column has various properties to control how it sizes itself and
              // how it positions its children. Here we use mainAxisAlignment to
              // center the children vertically; the main axis here is the vertical
              // axis because Columns are vertical (the cross axis would be
              // horizontal).
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                      'Reset Password',
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40.0),
                new Container(
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter your password';
                      }
                      return null;
                    },
                    controller: passwordController,
                    keyboardType: TextInputType.text,
                    obscureText: _showPassword,
                    focusNode: _passwordFocus,
                    textInputAction: TextInputAction.go,
                    style: TextStyle(
                        fontSize: 24.0, height: 1.0, color: Colors.black),
                    decoration: new InputDecoration(
                      labelText: 'Password',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color:
                              !this._showPassword ? Colors.blue : Colors.grey,
                        ),
                        onPressed: () {
                          setState(
                              () => this._showPassword = !this._showPassword);
                        },
                      ),
                    ),
                    onFieldSubmitted: (value) {
                      _passwordFocus.unfocus();
                      FocusScope.of(context).requestFocus(_cpasswordFocus);
                    },
                  ),
                ),
                SizedBox(height: 20.0),
                new Container(
                  child: TextFormField(
                    validator: (value) {
                      if (passwordController.text != value) {
                        return 'Password Mismatched';
                      }
                      return null;
                    },
                    controller: cpasswordController,
                    keyboardType: TextInputType.text,
                    obscureText: _showcPassword,
                    focusNode: _cpasswordFocus,
                    textInputAction: TextInputAction.go,
                    style: TextStyle(
                        fontSize: 24.0, height: 1.0, color: Colors.black),
                    decoration: new InputDecoration(
                      labelText: 'Confirm Password',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color:
                              !this._showcPassword ? Colors.blue : Colors.grey,
                        ),
                        onPressed: () {
                          setState(
                              () => this._showcPassword = !this._showcPassword);
                        },
                      ),
                    ),
                    onFieldSubmitted: (value) {
                      _passwordFocus.unfocus();
                      if (_formLoader.currentState.validate()) {
                        String password = passwordController.text;
                        onSubmitCallback(
                            mobileNumber, password, userType.toString());
                      }
                    },
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formLoader.currentState.validate()) {
                        String mobileNumber1 = "09" + mobileNumber;
                        String password = passwordController.text;
                        onSubmitCallback(
                            mobileNumber1, password, userType.toString());
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                              Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Update Password",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  onSubmitCallback(String mobileNumber, String password, String userType) {
    //Utils.showLoadingDialog(context, _state);
    UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest(
        mobileNumber: mobileNumber, password: password, userType: userType);
    new UserRepository().updatePassword(updatePasswordRequest).then((response) {
      if (response.code == 200) {
        //Utils.hideLoadingDialog(context);
        MyAlertDialog.showMyDialog(context, "Reset Password",
            "Password updated successfully, Please login to continue", () {
          Navigator.of(context).pop();
        });
      } else {
        MyAlertDialog.showMyDialog(context, "Reset Password",
            "Sorry, Unexpected error occurred. Please try again later.", null);
      }
    });
  }
}
