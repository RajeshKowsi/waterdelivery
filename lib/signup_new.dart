import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:styled_text/styled_text.dart';
import 'package:water/models/division_response.dart';
import 'package:water/models/signup_request.dart';
import 'package:water/models/signup_response.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:device_info/device_info.dart';
import 'package:water/models/township_request.dart';
import 'package:water/models/township_response.dart';
import 'package:water/myalert_dialog.dart';
import 'package:image_picker_type/image_picker_type.dart';
import 'package:water/register_step1.dart';
import 'package:water/styles/MyAppStyles.dart';

import 'common/utils.dart';

class SignupNewPage extends StatefulWidget {
  SignupNewPage({Key key}) : super(key: key);
  @override
  _MySignupPageState createState() => new _MySignupPageState();
}

class _MySignupPageState extends State<SignupNewPage> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  final GlobalKey<State> _state = new GlobalKey<State>();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  SignupResponse signupResponse;
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController agentCodeController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController cpasswordController = TextEditingController();

  final FocusNode firstNameFocus = FocusNode();
  final FocusNode lastNameFocus = FocusNode();
  final FocusNode mobileNumberFocus = FocusNode();
  final FocusNode agentCodeFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode cpasswordFocus = FocusNode();
  //bool _autovalidate = false;
  bool _showPassword = true;
  bool _showConfirmPassword = true;

  List<Division> divisionList = new List();
  List<Township> townshipList = new List();
  Division selectedDivision;
  Township selectedTownship;

  File _image;
  int userType = 0;

  void _handleUserTypeValueChange(int value) {
    setState(() {
      userType = value;
    });
  }

  initGalleryPickUp() async {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ImagePickerHelper(
            // isSave: true,  //if you want to save image in directory
            size: Size(300, 300),
            onDone: (file) {
              if (file == null) {
                print(null);
              } else {
                setState(() {
                  _image = file;
                });
              }
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    initPlatformState().then((value) {
      setState(() {});
    });
    new UserRepository().getDivision().then((divisionResponse) {
      print("divisionList ==>" + divisionResponse.toJson().toString());
      if (!divisionResponse.error) {
        setState(() {
          divisionList = divisionResponse.data;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final OtpPageParam otpPageParam = ModalRoute.of(context).settings.arguments;
    if (otpPageParam != null) {
      print("otpPageParams ==>" + otpPageParam.toString());
      mobileNumberController.text = otpPageParam.mobileNumber;
    } else {
      mobileNumberController.text = "123456789";
    }

    return new Scaffold(
        key: _state,
        appBar: AppBar(
          title: null,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.west_sharp,
              color: Colors.grey,
              size: 30,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Form(
            key: _formLoader,
            autovalidateMode: AutovalidateMode.always,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: new EdgeInsets.symmetric(horizontal: 30.0),
                      child: Text(
                        'Signup',
                        style: TextStyle(
                          fontSize: 34,
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  GestureDetector(
                      onTap: () {
                        print("ImagePicker Should be called");
                        initGalleryPickUp();
                      },
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 150,
                            height: 150,
                            margin: EdgeInsets.only(bottom: 5, top: 10),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.blue)),
                            child: Padding(
                              padding: EdgeInsets.all(6.0),
                              child: new CircleAvatar(
                                backgroundImage: (_image != null)
                                    ? FileImage(_image)
                                    : new AssetImage(
                                        "graphics/user_profile.png"),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 120, left: 100),
                            child: FloatingActionButton(
                              backgroundColor: const Color(
                                  MyAppStyles.BUTTON_THEME_END_COLOR),
                              foregroundColor: Colors.white,
                              onPressed: () {
                                print("ImagePicker Should be called");
                                initGalleryPickUp();
                              },
                              child: Icon(
                                Icons.add,
                                size: 50,
                              ),
                            ),
                          )
                        ],
                      )),
                  SizedBox(height: 20.0),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: new EdgeInsets.symmetric(horizontal: 30.0),
                      child: Text(
                        'Personal Information',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.black87,
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'First name is required field.';
                          }
                          return null;
                        },
                        focusNode: firstNameFocus,
                        controller: firstNameController,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'First Name',
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            hintText: ""),
                        onFieldSubmitted: (value) {
                          firstNameFocus.unfocus();
                          if (lastNameFocus != null) {
                            FocusScope.of(context).requestFocus(lastNameFocus);
                          }
                        }),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Last name is required field.';
                          }
                          return null;
                        },
                        focusNode: lastNameFocus,
                        controller: lastNameController,
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Last Name',
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            hintText: ""),
                        onFieldSubmitted: (value) {
                          lastNameFocus.unfocus();
                          if (mobileNumberFocus != null) {
                            FocusScope.of(context)
                                .requestFocus(mobileNumberFocus);
                          }
                        }),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Mobile number is required field.';
                          } else if (value.length > 10) {
                            return 'Pleae enter valid mobile number';
                          }
                          return null;
                        },
                        focusNode: mobileNumberFocus,
                        controller: mobileNumberController,
                        keyboardType: TextInputType.phone,
                        obscureText: false,
                        enabled: false,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Phone Number',
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            prefixText: '09 ',
                            prefixStyle: TextStyle(
                                fontSize: 22.0,
                                height: 1.0,
                                color: Colors.black),
                            hintText: ""),
                        onFieldSubmitted: (value) {
                          mobileNumberFocus.unfocus();
                          if (agentCodeFocus != null) {
                            FocusScope.of(context).requestFocus(agentCodeFocus);
                          }
                        }),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Text('I\'m a',
                            style: new TextStyle(fontSize: 20.0)),
                        new Radio(
                          value: 1,
                          groupValue: userType,
                          onChanged: _handleUserTypeValueChange,
                        ),
                        new GestureDetector(
                          onTap: () {
                            _handleUserTypeValueChange(1);
                          },
                          child: new Text(
                            'Customer',
                            style: new TextStyle(fontSize: 16.0),
                          ),
                        ),
                        new Radio(
                          value: 0,
                          groupValue: userType,
                          onChanged: _handleUserTypeValueChange,
                        ),
                        new GestureDetector(
                          onTap: () {
                            _handleUserTypeValueChange(0);
                          },
                          child: new Text(
                            'Agent',
                            style: new TextStyle(
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  // if (userType == 1)
                  //   new Container(
                  //     margin: new EdgeInsets.symmetric(horizontal: 30.0),
                  //     child: TextFormField(
                  //         validator: (value) {
                  //           if (userType == 1 && value.isEmpty) {
                  //             return 'Agent Mobile Number is required field.';
                  //           }
                  //           return null;
                  //         },
                  //         focusNode: agentCodeFocus,
                  //         controller: agentCodeController,
                  //         keyboardType: TextInputType.phone,
                  //         obscureText: false,
                  //         enabled: true,
                  //         textInputAction: TextInputAction.next,
                  //         style: TextStyle(
                  //             fontSize: 22.0, height: 1.0, color: Colors.black),
                  //         decoration: new InputDecoration(
                  //             labelText: 'Agent Phone Number',
                  //             floatingLabelBehavior:
                  //                 FloatingLabelBehavior.always,
                  //             prefixText: '09 ',
                  //             prefixStyle: TextStyle(
                  //                 fontSize: 22.0,
                  //                 height: 1.0,
                  //                 color: Colors.black),
                  //             hintText: ""),
                  //         onFieldSubmitted: (value) {
                  //           agentCodeFocus.unfocus();
                  //           if (passwordFocus != null) {
                  //             FocusScope.of(context)
                  //                 .requestFocus(passwordFocus);
                  //           }
                  //         }),
                  //   ),
                  // SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password is required field.';
                          }
                          return null;
                        },
                        focusNode: passwordFocus,
                        controller: passwordController,
                        keyboardType: TextInputType.text,
                        obscureText: _showPassword,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black),
                        decoration: new InputDecoration(
                          labelText: 'Password',
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: "",
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: !this._showPassword
                                  ? Colors.blue
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() =>
                                  this._showPassword = !this._showPassword);
                            },
                          ),
                        ),
                        onFieldSubmitted: (value) {
                          passwordFocus.unfocus();
                          if (cpasswordFocus != null) {
                            FocusScope.of(context).requestFocus(cpasswordFocus);
                          }
                        }),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password is required field.';
                          } else if (passwordController.text != value) {
                            return 'Password mis-matched';
                          }
                          return null;
                        },
                        focusNode: cpasswordFocus,
                        controller: cpasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: _showConfirmPassword,
                        textInputAction: TextInputAction.next,
                        style: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black),
                        decoration: new InputDecoration(
                          labelText: 'Confirm Password',
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: "",
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: !this._showConfirmPassword
                                  ? Colors.blue
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() => this._showConfirmPassword =
                                  !this._showConfirmPassword);
                            },
                          ),
                        ),
                        onFieldSubmitted: (value) {
                          cpasswordFocus.unfocus();
                        }),
                  ),
                  SizedBox(height: 20.0),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: new EdgeInsets.symmetric(horizontal: 30.0),
                      child: Text(
                        'Address Information',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.black87,
                          fontFamily: "Roboto",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: DropdownButtonFormField<Division>(
                      value: selectedDivision,
                      isExpanded: true,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 45,
                      decoration: InputDecoration(labelText: 'Your Division'),
                      onChanged: (Division division) {
                        setState(() {
                          print("selected Division ==> " +
                              division.toJson().toString());

                          if (selectedDivision != division) {
                            selectedTownship = null;
                            selectedDivision = division;
                            print("selectedDivision 2 ==> " +
                                selectedDivision.toJson().toString());

                            TownshipRequest request = new TownshipRequest(
                                divisionId: selectedDivision.id);

                            new UserRepository()
                                .getTownshipByDivisionId(request)
                                .then((townshipResponse) {
                              print("Township Response =" +
                                  townshipResponse.toJson().toString());

                              if (!townshipResponse.error) {
                                setState(() {
                                  townshipList = townshipResponse.data;
                                });
                              }
                            });
                          }
                        });
                      },
                      items: divisionList
                          .map<DropdownMenuItem<Division>>((Division division) {
                        return DropdownMenuItem<Division>(
                          value: division,
                          child: Container(
                            child: new Text(division.dName),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: DropdownButtonFormField<Township>(
                      value: selectedTownship,
                      isExpanded: true,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 45,
                      decoration: InputDecoration(labelText: 'Township'),
                      onChanged: (Township township) {
                        setState(() {
                          selectedTownship = township;
                        });
                      },
                      items: townshipList
                          .map<DropdownMenuItem<Township>>((Township township) {
                        return DropdownMenuItem<Township>(
                          value: township,
                          child: Container(
                            child: new Text(township.tName),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        _submitRegisterForm();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                                Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                              ],
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Sign up",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontFamily: "Roboto",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  new FlatButton(
                    key: null,
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/login');
                    },
                    child: StyledText(
                      text:
                          "<p>Already have an account, <purple>Log in</purple> now</p>",
                      styles: {
                        'p': TextStyle(
                            fontSize: 18.0,
                            color: Colors.grey,
                            fontFamily: "Roboto"),
                        'purple':
                            TextStyle(color: Color.fromRGBO(156, 119, 231, 1))
                      },
                    ),
                  ),
                  SizedBox(height: 40.0),
                ]),
          ),
        ));
  }

  _submitRegisterForm() {
    if (!_formLoader.currentState.validate()) {
      return;
    }

    if (selectedDivision == null || selectedTownship == null) {
      MyAlertDialog.showMyDialog(context, "Registration Failed",
          "Please select your division and township...", null);
      return;
    }

    Utils.showLoadingDialog(context, _state);
    if (_image != null) {
      new UserRepository().uploadImage(_image.path).then((response) {
        print("Image Uploaded url ==>" + response.data);
        _submitFormData(response.data);
      });
    } else {
      _submitFormData("");
    }
  }

  _submitFormData(String profilePicUrl) {
    String firstName = firstNameController.text;
    String lastName = lastNameController.text;
    String mobileNo = "09" + mobileNumberController.text;
    String agentCode = (userType == 1) ? "091234567890" : mobileNo;
    String passWordStr = passwordController.text;
    String division = selectedDivision.dName;
    String township = selectedTownship.tName;
    //String cpass = cpasswordController.text;
    print("Device Data ==> " + _deviceData["DeviceModel"]);
    SignupRequest signupRequest = new SignupRequest(
        firstName: firstName,
        lastName: lastName,
        mobileNumber: mobileNo,
        agentCode: agentCode,
        deviceModel: _deviceData["DeviceModel"],
        deviceMake: _deviceData["DeviceMake"],
        deviceType: _deviceData["DeviceType"],
        profileImage: profilePicUrl,
        deviceUniqueId: _deviceData["DeviceUniqueId"],
        password: passWordStr,
        userType: userType.toString(),
        isActive: "1",
        division: division,
        township: township,
        latitude: "0.0",
        longitude: "0.0");

    print("SignupRequest ==> " + signupRequest.toJson().toString());

    new UserRepository().registerUser(signupRequest).then((signupResponse) {
      Utils.hideLoadingDialog(context);
      if (signupResponse.code == 200) {
        MyAlertDialog.showMyDialog(
            context, "Water Delivery", "User Registration Successful", () {
          Navigator.pop(context);
        });
      } else {
        MyAlertDialog.showMyDialog(
            context, "Registration Failed", signupResponse.msg, null);
      }
    });
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
    if (!mounted) return;
    _deviceData = deviceData;
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'DeviceMake': build.brand,
      'DeviceModel': build.device,
      'DeviceUniqueId': build.androidId,
      'DeviceType': "Android"
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'DeviceMake': data.name,
      'DeviceModel': data.model,
      'DeviceUniqueId': data.identifierForVendor,
      'DeviceType': "iOS",
    };
  }
}
