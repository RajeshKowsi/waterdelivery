import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:water/register_step1.dart';
import 'package:flutter/widgets.dart';
import 'package:water/styles/MyAppStyles.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class RegisterStepTwoPage extends StatefulWidget {
  @override
  _RegisterStepTwoPageState createState() => _RegisterStepTwoPageState();
}

class _RegisterStepTwoPageState extends State<RegisterStepTwoPage> {
  final GlobalKey<State> _state = new GlobalKey<State>();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  //bool _autovalidate = false;

  TextEditingController mobileNumberController = TextEditingController();
  final FocusNode mobileNumberFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final OtpPageParam otpPageParam = ModalRoute.of(context).settings.arguments;
    print("otpPageParams ==>" + otpPageParam.toString());

    String otpNumber = otpPageParam.otpNumber;

    return new Scaffold(
      backgroundColor: Colors.white,
      key: _state,
      appBar: AppBar(
        title: null,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.west_sharp,
            color: Colors.grey,
            size: 30,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
          child: Form(
        key: _formLoader,
        autovalidateMode: AutovalidateMode.always,
        child: Padding(
          padding: EdgeInsets.only(
              left: MyAppStyles.PAGE_PADDING, right: MyAppStyles.PAGE_PADDING),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  'assets/images/otp_image.png',
                  height: MediaQuery.of(context).size.height * 0.45,
                ),
                SizedBox(height: 1.0),
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'OTP Code',
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.black,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                PinEntryTextField(
                  onSubmit: (String pin) {
                    if (pin == otpNumber) {
                      var pageTarget = (otpPageParam.flow == "forgotpwd")
                          ? "/forgotpwd"
                          : "/register_details";
                      Navigator.pushReplacementNamed(context, pageTarget,
                          arguments: otpPageParam);
                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Error!"),
                              content:
                                  Text("Invalid OTP... Please try again..."),
                              actions: <Widget>[
                                IconButton(
                                    icon: Text("OK"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    })
                              ],
                            );
                          });
                    }
                  }, // end onSubmit
                ),
                SizedBox(height: 10.0),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'Please, enter 4 - digit code we sent on your number as SMS',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Error!"),
                              content:
                                  Text("Invalid OTP... Please try again..."),
                              actions: <Widget>[
                                IconButton(
                                    icon: Text("OK"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    })
                              ],
                            );
                          });
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                              Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Verify",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      )),
    );
  }

  String generateRandomOTP() {
    var rndnumber = "";
    var rnd = new Random();
    for (var i = 0; i < 6; i++) {
      rndnumber = rndnumber + rnd.nextInt(9).toString();
    }
    return rndnumber;
  }
}
