// Flutter code sample for ListTile

// Here is an example of an article list item with multiline titles and
// subtitles. It utilizes [Row]s and [Column]s, as well as [Expanded] and
// [AspectRatio] widgets to organize its layout.
//
// ![Custom list item b](https://flutter.github.io/assets-for-api-docs/assets/widgets/custom_list_item_b.png)

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/map_utils.dart';
import 'package:water/models/login_response.dart';
import 'package:water/models/myorder_request.dart';
import 'package:water/models/myorder_response.dart';
import 'package:water/models/update_order_request.dart';
import 'package:water/myorder_direction.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:intl/intl.dart';

import 'common/utils.dart';

class MyOrdersListItem extends StatelessWidget {
  MyOrdersListItem(
      {Key key,
      this.orderId,
      this.agentName,
      this.subtitle,
      this.brandName,
      this.orderDate,
      this.readDuration,
      this.count,
      this.deliveryStatus})
      : super(key: key);

  final String orderId;
  final String agentName;
  final String subtitle;
  final String brandName;
  final String orderDate;
  final String readDuration;
  final bool deliveryStatus;
  final int count;

  String getDeliverStatus(bool deliveryStatus) {
    return (deliveryStatus) ? "DELIVERED" : "PENDING";
  }

  Color getStatusColor(bool deliveryStatus) {
    return (deliveryStatus) ? Colors.green : Colors.orange;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10.0),
              Text(
                '$agentName'.toUpperCase(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                getDeliverStatus(deliveryStatus),
                style: TextStyle(
                  fontSize: 14.0,
                  color: getStatusColor(deliveryStatus),
                ),
              ),
              SizedBox(height: 20.0),
              Text(
                '$orderDate',
                style: const TextStyle(
                  fontSize: 14.0,
                  color: Colors.blueAccent,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class MyOrderListItemWrapper extends StatelessWidget {
  MyOrderListItemWrapper({Key key, this.thumbnail, this.myOrder, this.userType})
      : super(key: key);

  final Widget thumbnail;
  final MyOrder myOrder;
  final int userType;

  String getFormattedDateString(String dateString) {
    String output = "";
    var d = DateTime.parse(dateString);
    output = new DateFormat("MMM dd, yyyy hh:ss").format(d);
    return output;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: SizedBox(
        height: 150,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 90,
              height: 90,
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                    image: AssetImage("assets/images/bottle1.jpg"),
                    fit: BoxFit.fill),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: MyOrdersListItem(
                    orderId: myOrder.id,
                    agentName: (userType == 1)
                        ? myOrder.agent.firstname + " " + myOrder.agent.lastname
                        : myOrder.agent.firstname +
                            " " +
                            myOrder.agent.lastname,
                    subtitle: myOrder.count.toString(),
                    brandName: myOrder.brandName,
                    orderDate: getFormattedDateString(myOrder.createdAt),
                    readDuration: "Ks" + myOrder.amount.toString(),
                    deliveryStatus: myOrder.deliveryStatus,
                    count: myOrder.count),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyOrdersNewPage extends StatefulWidget {
  @override
  _MyOrdersNewPageState createState() => _MyOrdersNewPageState();
}

class _MyOrdersNewPageState extends State<MyOrdersNewPage> {
  // _MyOrdersState({Key key}) : super(key: key);
  LoginResponse userDetail;
  List<MyOrder> myOrderList = new List<MyOrder>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _getUserDetails().then((value) => null);
  }

  Future<void> _getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    //print("####### user details ===>" + userString);
    setState(() {
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      print("####### user mobileNumber ===>" + userDetail.data.userId);
      print("####### user userType ===>" + userDetail.data.userType.toString());
      getMyOrdersList().then((value) => null);
    });
  }

  Future<void> getMyOrdersList() async {
    MyOrderRequest orderRequest = new MyOrderRequest(
        userId: userDetail.data.userId,
        userType: userDetail.data.userType.toString());
    print("MyOrder orderRequest ==>" + orderRequest.toJson().toString());

    new UserRepository()
        .getMyOrderHistory(orderRequest)
        .then((myOrderResponse) {
      setState(() {
        if (!myOrderResponse.error) {
          myOrderList = myOrderResponse.data;
          myOrderList.sort((a, b) {
            if (a.deliveryStatus != b.deliveryStatus)
              return 1;
            else if (a.deliveryStatus)
              return -1;
            else {
              return DateTime.parse(b.createdAt)
                  .compareTo(DateTime.parse(a.createdAt));
            }
          });
        } else {
          myOrderList = new List<MyOrder>();
        }
      });
    });
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getMyOrdersList().then((value) => null);
    if (mounted) _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop();
        return;
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: null,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.west_sharp,
              color: Colors.grey,
              size: 30,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus mode) {
              Widget body;
              if (mode == LoadStatus.idle) {
                body = Text("");
              } else if (mode == LoadStatus.loading) {
                body = CupertinoActivityIndicator();
              } else if (mode == LoadStatus.failed) {
                body = Text("Load Failed!Click retry!");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("");
              } else {
                body = Text("No more Data");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: myOrderList.isEmpty
              ? Center(child: Text('No Orders Available!'))
              : ListView.builder(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  itemCount: myOrderList.length,
                  itemBuilder: (BuildContext ctxt, int index) {
                    return GestureDetector(
                        child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      elevation: 5,
                      child: Stack(
                        children: [
                          MyOrderListItemWrapper(
                              myOrder: myOrderList[index],
                              userType: userDetail.data.userType),
                          Align(
                            alignment: Alignment.topLeft,
                            child: new Container(
                              height: 60,
                              margin: EdgeInsets.only(top: 110, left: 5),
                              child: ButtonBar(
                                alignment: MainAxisAlignment.start,
                                overflowButtonSpacing: 1.0,
                                children: <Widget>[
                                  Card(
                                      color: Color.fromRGBO(243, 243, 243, 1),
                                      elevation: 5,
                                      child: new FlatButton(
                                        child: const Text(
                                          'Call',
                                          style: TextStyle(
                                            color: Colors.black,
                                          ),
                                        ),
                                        onPressed: () {
                                          print("Call button clicked");
                                          /* ... */
                                          String mobileUri = "tel:" +
                                              myOrderList[index].agent.mobileno;
                                          MapUtils.openUri(mobileUri);
                                        },
                                      )),
                                  if (userDetail.data.userType == 0)
                                    Card(
                                        color: Color.fromRGBO(243, 243, 243, 1),
                                        elevation: 5,
                                        child: new FlatButton(
                                          child: const Text(
                                            'Direction',
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                          onPressed: () {
                                            print(
                                                "Direction button clicked location =" +
                                                    myOrderList[index]
                                                        .agent
                                                        .latitude +
                                                    " , " +
                                                    myOrderList[index]
                                                        .agent
                                                        .longitude);

                                            if (myOrderList[index]
                                                        .agent
                                                        .latitude !=
                                                    null &&
                                                myOrderList[index]
                                                        .agent
                                                        .longitude !=
                                                    null) {
                                              Navigator.pushNamed(
                                                  context, "/myorderdirection",
                                                  arguments:
                                                      new BookingDirectionParams(
                                                          myOrder: myOrderList[
                                                              index]));
                                            }
                                          },
                                        )),
                                  if (userDetail.data.userType == 0)
                                    Card(
                                        elevation: 5,
                                        color: Color.fromRGBO(243, 243, 243, 1),
                                        child: new FlatButton(
                                          child: const Text(
                                            'Close Order',
                                            style: TextStyle(
                                              color: Colors.black,
                                            ),
                                          ),
                                          onPressed: () {
                                            print("Close Order Called");
                                            UpdateOrderRequest request =
                                                new UpdateOrderRequest(
                                                    orderId:
                                                        myOrderList[index].id,
                                                    orderStatus: true);
                                            print("UpdateOrderRequest ==>" +
                                                request.toJson().toString());
                                            new UserRepository()
                                                .updateOrder(request)
                                                .then((response) {
                                              if (!response.error) {
                                                Navigator.pushReplacementNamed(
                                                    context, '/myorders');
                                              }
                                            });
                                          },
                                        )),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Card(
                              color: Colors.blue,
                              elevation: 20,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.vertical(
                                    bottom: Radius.circular(15)),
                              ),
                              margin: EdgeInsets.only(right: 20),
                              child: Container(
                                width: 70,
                                height: 80,
                                child: Column(
                                  children: [
                                    SizedBox(height: 20.0),
                                    Text(
                                      myOrderList[index].count.toString() +
                                          " Cans",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 10.0),
                                    Text(
                                      myOrderList[index].amount.toString() +
                                          " Ks",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ));
                  },
                ),
        ),
      ),
    );
  }
}
