import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/common/utils.dart';
import 'package:water/register_step1.dart';
import 'package:water/styles/MyAppStyles.dart';
import 'bloc/user_repository.dart';
import 'models/login_request.dart';
import 'myalert_dialog.dart';

class LoginNewPage extends StatefulWidget {
  LoginNewPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _LoginNewPageState createState() => _LoginNewPageState();
}

class _LoginNewPageState extends State<LoginNewPage> {
  final GlobalKey<State> _state = new GlobalKey<State>();
  final FocusNode _mobileFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  final TextEditingController mobileController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  String mobileNumber;
  bool _showPassword = true;

  int rememberMe = 0;
  @override
  Widget build(BuildContext context) {
    final LoginParam loginParam = ModalRoute.of(context).settings.arguments;

    if (loginParam != null) {
      print("LoginParam ==>" + loginParam.toString());
      mobileNumber = loginParam.mobileNumber;
      mobileController.text = mobileNumber;
    }
    getUserName().then((String result) {
      if (result != null) {
        setState(() {
          rememberMe = 1;
        });
        mobileNumber = result;
        mobileController.text = mobileNumber;
      }
    });

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: null,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.west_sharp,
            color: Colors.grey,
            size: 30,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Form(
        key: _formLoader,
        child: Padding(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.

            child: Column(
              // Column is also a layout widget. It takes a list of children and
              // arranges them vertically. By default, it sizes itself to fit its
              // children horizontally, and tries to be as tall as its parent.
              //
              // Invoke "debug painting" (press "p" in the console, choose the
              // "Toggle Debug Paint" action from the Flutter Inspector in Android
              // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
              // to see the wireframe for each widget.
              //
              // Column has various properties to control how it sizes itself and
              // how it positions its children. Here we use mainAxisAlignment to
              // center the children vertically; the main axis here is the vertical
              // axis because Columns are vertical (the cross axis would be
              // horizontal).
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                      'Login',
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40.0),
                new Container(
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty || value.length < 9) {
                        return 'Please enter valid mobile number';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    focusNode: _mobileFocus,
                    controller: mobileController,
                    style: TextStyle(
                        fontSize: 22.0, height: 1.0, color: Colors.black),
                    textInputAction: TextInputAction.next,
                    decoration: new InputDecoration(
                        labelText: 'Phone Number',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        prefixText: '09 ',
                        prefixStyle: TextStyle(
                            fontSize: 22.0, height: 1.0, color: Colors.black)),
                    onFieldSubmitted: (value) {
                      _fieldFocusChange(context, _mobileFocus, _passwordFocus);
                    },
                  ),
                ),
                SizedBox(height: 20.0),
                new Container(
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter your password';
                      }
                      return null;
                    },
                    controller: passwordController,
                    keyboardType: TextInputType.text,
                    obscureText: _showPassword,
                    focusNode: _passwordFocus,
                    textInputAction: TextInputAction.go,
                    style: TextStyle(
                        fontSize: 24.0, height: 1.0, color: Colors.black),
                    decoration: new InputDecoration(
                      labelText: 'Password',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color:
                              !this._showPassword ? Colors.blue : Colors.grey,
                        ),
                        onPressed: () {
                          setState(
                              () => this._showPassword = !this._showPassword);
                        },
                      ),
                    ),
                    onFieldSubmitted: (value) {
                      _passwordFocus.unfocus();
                      if (_formLoader.currentState.validate()) {
                        String mobileNumber = "09" + mobileController.text;
                        String password = passwordController.text;
                        onSubmitCallback(mobileNumber, password);
                      }
                    },
                  ),
                ),
                SizedBox(height: 10.0),
                new Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 180,
                        child: ListTile(
                          title: Align(
                            child: new Text(
                              'Remember Me',
                              style: new TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.grey,
                                  fontFamily: "Roboto"),
                            ),
                            alignment: Alignment(-10.0, 0),
                          ),
                          leading: Radio(
                            value: rememberMe,
                            groupValue: 1,
                            onChanged: (int value) {
                              setState(() {
                                rememberMe = 1;
                              });
                            },
                          ),
                          onTap: () {
                            setState(() {
                              rememberMe = 1;
                            });
                          },
                        ),
                      ),
                      new FlatButton(
                          key: null,
                          onPressed: () {
                            Navigator.pushNamed(context, '/register',
                                arguments: {"flow": "forgotpwd"});
                          },
                          child: new Text(
                            "Forgot Password?",
                            style: new TextStyle(
                                fontSize: 12.0,
                                color: Color.fromRGBO(156, 119, 231, 1),
                                fontFamily: "Roboto"),
                          ))
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formLoader.currentState.validate()) {
                        String mobileNumber = "09" + mobileController.text;
                        String password = passwordController.text;
                        onSubmitCallback(mobileNumber, password);
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                              Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Login",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                new FlatButton(
                    key: null,
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/register',
                          arguments: {"flow": "register"});
                    },
                    child: StyledText(
                      text:
                          "<p>Don't have an Account? <purple>Sign up</purple></p>",
                      styles: {
                        'p': TextStyle(
                            fontSize: 18.0,
                            color: Colors.grey,
                            fontFamily: "Roboto"),
                        'purple':
                            TextStyle(color: Color.fromRGBO(156, 119, 231, 1))
                      },
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  onSubmitCallback(String mobileNumber, String password) {
    Utils.showLoadingDialog(context, _state);
    print("RememberMe ===> " + rememberMe.toString());
    if (rememberMe == 1) {
      rememberUserName(mobileController.text);
    }
    LoginRequest loginRequest =
        new LoginRequest(mobileNumber: mobileNumber, password: password);
    new UserRepository().loginUser(loginRequest).then((loginResponse) {
      Utils.hideLoadingDialog(context);
      if (!loginResponse.error) {
        saveUserDetails(jsonEncode(loginResponse.toJson()));
        //Navigator.pushReplacementNamed(context, '/home');
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
      } else {
        MyAlertDialog.showMyDialog(
            context, "Login Failed", loginResponse.msg, null);
      }
    });
  }

  Future<void> saveUserDetails(String userData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userdetails", userData);
  }

  Future<void> rememberUserName(String username) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("username", username);
  }

  Future<String> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString("username");
    return username;
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
