import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:water/models/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/myprofile.dart';
import 'common/utils.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);
  @override
  _MySignupPageState createState() => new _MySignupPageState();
}

class _MySignupPageState extends State<ProfilePage> {
  LoginResponse userDetail;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    getUserDetails().then((value) {});
  }

  Future<void> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      print("profile userDetail =>" + userString);
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      print("profile userDetail =>" + userDetail.toJson().toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop();
        return;
      },
      child: new Scaffold(
        key: _scaffoldKey,
        body: new MyProfilePage(userDetail: userDetail),
      ),
    );
  }
}
