class MyAppStyles {
  static const double PAGE_PADDING = 16.0;

  //FONT SIZE
  //static const double PAGE_PADDING = 8.0;
  static const double GUEST_PAGE_LR_PADDING = 8.0;
  static const int BUTTON_THEME_START_COLOR = 0xff667EEA;
  static const int BUTTON_THEME_END_COLOR = 0xff65B2FE;
}
