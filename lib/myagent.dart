import 'package:flutter/material.dart';
import 'DrawerCode.dart';

class MyAgentPage extends StatefulWidget {
  @override
  _MyAgentPageState createState() => _MyAgentPageState();
}

class _MyAgentPageState extends State<MyAgentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Agent Details"),
      ),
      drawer: DrawerCode(),
      body: Center(
        child: Text(
            "Default agent not picked yet. Please pick form home page..."), // Then here is setting page
      ),
    );
  }
}
