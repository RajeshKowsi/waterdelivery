import 'package:water/networking/CustomException.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:connectivity/connectivity.dart';

class ApiProvider {
  //final String _baseUrl = "http://54.169.253.240:3000/api/";
  final String _baseUrl = "http://54.151.157.35:3000/api/";

  Future<dynamic> get(String url) async {
    var responseJson;
    var requestUrl = (url.contains("http://")) ? url : _baseUrl + url;
    try {
      final response = await http.get(
        requestUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
      );
      responseJson = _response(response);
    } on SocketException {
      //throw FetchDataException('No Internet connection');
      responseJson = {
        "code": "0",
        "error": true,
        "msg": "NO_INTERNET",
      };
    }
    return responseJson;
  }

  Future<dynamic> post(String url, dynamic data) async {
    var responseJson;
    var requestUrl = (url.contains("http://")) ? url : _baseUrl + url;
    try {
      final response = await http.post(
        requestUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        body: jsonEncode(data),
      );
      responseJson = _response(response);
    } on SocketException {
      //throw FetchDataException('No Internet connection');
      responseJson = {
        "code": "0",
        "error": true,
        "msg": "NO_INTERNET",
      };
    }
    return responseJson;
  }

  Future<dynamic> uploadImage(String url, String filename) async {
    try {
      var request = http.MultipartRequest('POST', Uri.parse(_baseUrl + url));
      request.files.add(await http.MultipartFile.fromPath("picture", filename));
      var res = await request.send();
      //print("uploadImage Response =" + res.toString());
      var response = await http.Response.fromStream(res);
      //final respStr = await res.stream.bytesToString();
      return _response(response);
    } on SocketException {
      //throw FetchDataException('No Internet connection');
      return {
        "code": "0",
        "error": true,
        "msg": "NO_INTERNET",
      };
    }
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}
