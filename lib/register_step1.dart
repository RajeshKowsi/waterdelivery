import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/models/send_sms_agent.dart';
import 'package:water/models/user_exists_request.dart';
import 'package:water/myalert_dialog.dart';
import 'package:water/styles/MyAppStyles.dart';

class RegisterStepOnePage extends StatefulWidget {
  @override
  _RegisterStepOnePageState createState() => _RegisterStepOnePageState();
}

class _RegisterStepOnePageState extends State<RegisterStepOnePage> {
  final GlobalKey<State> _state = new GlobalKey<State>();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  //bool _autovalidate = false;
  Map<String, dynamic> myParams;

  TextEditingController mobileNumberController = TextEditingController();
  final FocusNode mobileNumberFocus = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    myParams = ModalRoute.of(context).settings.arguments;
    print("MyParams ==>" + myParams.toString());

    String title =
        (myParams["flow"] == "forgotpwd") ? "Forgot Password" : "Registration";
    String imageUrl = (myParams["flow"] == "forgotpwd")
        ? "assets/images/forgot_pwd.png"
        : "assets/images/register_icon_step1.png";
    return new Scaffold(
      backgroundColor: Colors.white,
      key: _state,
      appBar: AppBar(
        title: null,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.west_sharp,
            color: Colors.grey,
            size: 30,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
          child: Form(
        key: _formLoader,
        autovalidateMode: AutovalidateMode.always,
        child: Padding(
          padding: EdgeInsets.only(
              left: MyAppStyles.PAGE_PADDING, right: MyAppStyles.PAGE_PADDING),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  imageUrl,
                  height: MediaQuery.of(context).size.height * 0.45,
                ),
                SizedBox(height: 1.0),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: 34,
                        color: Colors.black,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'Enter Your Mobile Number',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    margin: new EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'To Receive a Verification Code',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                        fontFamily: "Roboto",
                      ),
                    ),
                  ),
                ),
                new Container(
                  width: MediaQuery.of(context).size.width * 0.80,
                  child: Align(
                    alignment: Alignment.center,
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty || value.length < 9) {
                          return 'Please enter valid mobile number';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      focusNode: mobileNumberFocus,
                      controller: mobileNumberController,
                      style: TextStyle(
                          fontSize: 20.0, height: 1.0, color: Colors.black),
                      textInputAction: TextInputAction.next,
                      decoration: new InputDecoration(
                          hintText: '',
                          prefixText: '09 ',
                          prefixStyle: TextStyle(
                              fontSize: 20.0,
                              height: 1.0,
                              color: Colors.black)),
                      onFieldSubmitted: (value) {
                        mobileNumberFocus.unfocus();
                        onSubmitCallback(mobileNumberController.text);
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formLoader.currentState.validate()) {
                        onSubmitCallback(mobileNumberController.text);
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(MyAppStyles.BUTTON_THEME_START_COLOR),
                              Color(MyAppStyles.BUTTON_THEME_END_COLOR)
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Get Code",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: "Roboto",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      )),
    );
  }

  String generateRandomOTP() {
    var rndnumber = "";
    var rnd = new Random();
    for (var i = 0; i < 4; i++) {
      rndnumber = rndnumber + rnd.nextInt(9).toString();
    }
    return rndnumber;
  }

  onSubmitCallback(String mobileNumber) {
    //Utils.showLoadingDialog(context, _state);
    if (!_formLoader.currentState.validate()) {
      return;
    }

    String otpNumber = generateRandomOTP();
    String smsContent = otpNumber + "- Use this code for WaterApp verification";
    print("smsContent ==  >" + smsContent);

    UserExistsRequest userExistsRequest = new UserExistsRequest(
        mobileNumber: "00959" + mobileNumber, smsContent: smsContent);
    new UserRepository().checkUserExists(userExistsRequest).then((response) {
      print("check User Exists ==>" + response.toJson().toString());
      if (response.code == 300) {
        if (myParams["flow"] != "forgotpwd") {
          sendOTP(mobileNumber, otpNumber, smsContent, response.data.userType);
        } else {
          MyAlertDialog.showMyDialog(
              context, "Water App", "User Account not Found!", null);
        }
      } else if (response.code == 200) {
        if (myParams["flow"] == "forgotpwd") {
          sendOTP(mobileNumber, otpNumber, smsContent, response.data.userType);
        } else {
          Navigator.pushReplacementNamed(context, '/login',
              arguments: new LoginParam(mobileNumber: mobileNumber));
        }
      }
    });
  }

  sendOTP(
      String mobileNumber, String otpNumber, String smsContent, int userType) {
    SendSMSRequest request = new SendSMSRequest(
        mobileNumber: "00959" + mobileNumber, smsContent: smsContent);
    print("SEND SMS REQUEST ==  >" + request.toJson().toString());
    new UserRepository().sendSMS(request).then((response) {
      print("SEND SMS RESPONSE ==  >" + response.toJson().toString());
      Navigator.pushReplacementNamed(context, '/registerotp',
          arguments: new OtpPageParam(
              mobileNumber: mobileNumber,
              otpNumber: otpNumber,
              flow: this.myParams["flow"],
              userType: userType));
    });
  }
}

class LoginParam {
  String mobileNumber;
  LoginParam({this.mobileNumber});
}

class OtpPageParam {
  String mobileNumber;
  String otpNumber;
  String flow;
  int userType;
  OtpPageParam({this.mobileNumber, this.otpNumber, this.flow, this.userType});
}
