import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyAlertDialog {
  static Future<void> showMyDialog(BuildContext context, String title,
      String message, Function callback) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[Text(message)],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                if (callback != null) {
                  callback();
                }
              },
            ),
          ],
        );
      },
    );
  }

  static Future<void> showLocationChangeDialog(
      BuildContext context,
      String title,
      String message,
      Function callbackYes,
      Function callbackNo) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[Text(message)],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('NO'),
              onPressed: () {
                Navigator.of(context).pop(false);
                if (callbackNo != null) {
                  callbackNo();
                }
              },
            ),
            FlatButton(
              child: Text('YES'),
              onPressed: () {
                Navigator.of(context).pop(true);
                if (callbackYes != null) {
                  callbackYes();
                }
              },
            ),
          ],
        );
      },
    );
  }
}
