// Flutter code sample for ListTile

// Here is an example of an article list item with multiline titles and
// subtitles. It utilizes [Row]s and [Column]s, as well as [Expanded] and
// [AspectRatio] widgets to organize its layout.
//
// ![Custom list item b](https://flutter.github.io/assets-for-api-docs/assets/widgets/custom_list_item_b.png)

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/map_utils.dart';
import 'package:water/models/login_response.dart';
import 'package:water/models/myorder_request.dart';
import 'package:water/models/myorder_response.dart';
import 'package:water/models/update_order_request.dart';
import 'package:water/myorder_direction.dart';
import 'DrawerCode.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:intl/intl.dart';

import 'common/utils.dart';

class MyOrdersListItem extends StatelessWidget {
  MyOrdersListItem(
      {Key key,
      this.orderId,
      this.agentName,
      this.subtitle,
      this.brandName,
      this.orderDate,
      this.readDuration,
      this.deliveryStatus})
      : super(key: key);

  final String orderId;
  final String agentName;
  final String subtitle;
  final String brandName;
  final String orderDate;
  final String readDuration;
  final bool deliveryStatus;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$agentName'.toUpperCase(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 2.0)),
              Text(
                '$brandName',
                style: const TextStyle(
                  fontSize: 12.0,
                  color: Colors.black87,
                ),
              ),
              Text(
                '$orderDate',
                style: const TextStyle(
                  fontSize: 12.0,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class MyOrderListItemWrapper extends StatelessWidget {
  MyOrderListItemWrapper({Key key, this.thumbnail, this.myOrder, this.userType})
      : super(key: key);

  final Widget thumbnail;
  final MyOrder myOrder;
  final int userType;

  String getFormattedDateString(String dateString) {
    String output = "";
    var d = DateTime.parse(dateString);
    output = new DateFormat("MMM dd, yyyy hh:ss").format(d);
    return output;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: SizedBox(
        height: 70,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 50,
              height: 60,
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                    image: NetworkImage(
                        "https://waterlalo1.s3-ap-southeast-1.amazonaws.com/aqua.jpeg"),
                    fit: BoxFit.fill),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 2.0, 0.0),
                child: MyOrdersListItem(
                  orderId: myOrder.id,
                  agentName: (userType == 1)
                      ? myOrder.agent.firstname + " " + myOrder.agent.lastname
                      : myOrder.agent.firstname + " " + myOrder.agent.lastname,
                  subtitle: myOrder.count.toString(),
                  brandName: myOrder.brandName,
                  orderDate: getFormattedDateString(myOrder.createdAt),
                  readDuration: "Ks" + myOrder.amount.toString(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyOrdersPage extends StatefulWidget {
  @override
  _MyOrdersPageState createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  // _MyOrdersState({Key key}) : super(key: key);
  LoginResponse userDetail;
  List<MyOrder> myOrderList = new List<MyOrder>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _getUserDetails().then((value) => null);
  }

  Future<void> _getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    //print("####### user details ===>" + userString);
    setState(() {
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      print("####### user mobileNumber ===>" + userDetail.data.userId);
      print("####### user userType ===>" + userDetail.data.userType.toString());
      getMyOrdersList().then((value) => null);
    });
  }

  Future<void> getMyOrdersList() async {
    MyOrderRequest orderRequest = new MyOrderRequest(
        userId: userDetail.data.userId,
        userType: userDetail.data.userType.toString());
    print("MyOrder orderRequest ==>" + orderRequest.toJson().toString());

    new UserRepository()
        .getMyOrderHistory(orderRequest)
        .then((myOrderResponse) {
      setState(() {
        if (!myOrderResponse.error) {
          myOrderList = myOrderResponse.data;
          myOrderList.sort((a, b) {
            if (a.deliveryStatus != b.deliveryStatus) if (a.deliveryStatus)
              return 1;
            else
              return -1;
            else {
              return DateTime.parse(b.createdAt)
                  .compareTo(DateTime.parse(a.createdAt));
            }
          });
        } else {
          myOrderList = new List<MyOrder>();
        }
      });
    });
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getMyOrdersList().then((value) => null);
    if (mounted) _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Utils.onBackPressed(context, _scaffoldKey),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("My Orders Page"),
        ),
        drawer: DrawerCode(),
        body: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus mode) {
              Widget body;
              if (mode == LoadStatus.idle) {
                body = Text("");
              } else if (mode == LoadStatus.loading) {
                body = CupertinoActivityIndicator();
              } else if (mode == LoadStatus.failed) {
                body = Text("Load Failed!Click retry!");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("");
              } else {
                body = Text("No more Data");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: myOrderList.isEmpty
              ? Center(child: Text('No Orders Available!'))
              : ListView.builder(
                  padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                  itemCount: myOrderList.length,
                  itemBuilder: (BuildContext ctxt, int index) {
                    return GestureDetector(
                        child: Card(
                      elevation: 5,
                      child: Stack(children: [
                        MyOrderListItemWrapper(
                            myOrder: myOrderList[index],
                            userType: userDetail.data.userType),
                        Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            width: 90,
                            alignment: Alignment.topRight,
                            margin: EdgeInsets.only(top: 0, right: 5),
                            child: ClipRect(
                              child: Stack(children: [
                                Image.asset("graphics/price_tag.png",
                                    fit: BoxFit.cover),
                                new Container(
                                    padding: EdgeInsets.only(left: 14, top: 6),
                                    alignment: Alignment.center,
                                    child: Row(children: [
                                      Column(children: [
                                        Text(
                                          myOrderList[index].count.toString(),
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                        Text(
                                          "Cans",
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ]),
                                      new Container(
                                          padding: new EdgeInsets.only(
                                              left: 12, top: 5),
                                          alignment: Alignment.center,
                                          child: Text(
                                              "Ks" +
                                                  myOrderList[index]
                                                      .amount
                                                      .toString(),
                                              softWrap: true,
                                              style: const TextStyle(
                                                  fontSize: 9.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white)))
                                    ]))
                              ]),
                            ),
                          ),
                        ),
                        Align(
                            alignment: Alignment.topLeft,
                            child: new Container(
                              height: 60,
                              margin: EdgeInsets.only(top: 70, left: 40),
                              child: ButtonBar(
                                alignment: MainAxisAlignment.start,
                                overflowButtonSpacing: 1.0,
                                children: <Widget>[
                                  Card(
                                      elevation: 5,
                                      child: new FlatButton(
                                        child: const Text('Call'),
                                        onPressed: () {
                                          print("Call button clicked");
                                          /* ... */
                                          String mobileUri = "tel:" +
                                              myOrderList[index].agent.mobileno;
                                          MapUtils.openUri(mobileUri);
                                        },
                                      )),
                                  if (userDetail.data.userType == 0)
                                    Card(
                                        elevation: 5,
                                        child: new FlatButton(
                                          child: const Text('Direction'),
                                          onPressed: () {
                                            print(
                                                "Direction button clicked location =" +
                                                    myOrderList[index]
                                                        .agent
                                                        .latitude +
                                                    " , " +
                                                    myOrderList[index]
                                                        .agent
                                                        .longitude);

                                            if (myOrderList[index]
                                                        .agent
                                                        .latitude !=
                                                    null &&
                                                myOrderList[index]
                                                        .agent
                                                        .longitude !=
                                                    null) {
                                              Navigator.pushNamed(
                                                  context, "/myorderdirection",
                                                  arguments:
                                                      new BookingDirectionParams(
                                                          myOrder: myOrderList[
                                                              index]));
                                            }
                                          },
                                        )),
                                  if (userDetail.data.userType == 0)
                                    Card(
                                        elevation: 5,
                                        child: new FlatButton(
                                          child: const Text('Close Order'),
                                          onPressed: () {
                                            print("Close Order Called");
                                            UpdateOrderRequest request =
                                                new UpdateOrderRequest(
                                                    orderId:
                                                        myOrderList[index].id,
                                                    orderStatus: true);
                                            new UserRepository()
                                                .updateOrder(request)
                                                .then((response) {
                                              if (!response.error) {
                                                Navigator.pushReplacementNamed(
                                                    context, '/myorders');
                                              }
                                            });
                                          },
                                        )),
                                ],
                              ),
                            )),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                                margin: EdgeInsets.only(top: 55, right: 25),
                                child: new Text(
                                  getDeliverStatus(
                                      myOrderList[index].deliveryStatus),
                                  style: TextStyle(
                                      color: getStatusColor(
                                          myOrderList[index].deliveryStatus),
                                      fontWeight: FontWeight.bold),
                                )))
                      ]),
                    ));
                  },
                ),
        ),
      ),
    );
  }

  String getDeliverStatus(bool deliveryStatus) {
    return (deliveryStatus) ? "DELIVERED" : "PENDING";
  }

  Color getStatusColor(bool deliveryStatus) {
    return (deliveryStatus) ? Colors.green : Colors.orange;
  }
}
