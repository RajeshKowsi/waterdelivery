import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:water/models/login_response.dart';
import 'package:google_maps_webservice/places.dart' as GooglPlaces;
import 'package:water/models/nearbyagent_response.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'bloc/user_repository.dart';
import 'models/myorder_response.dart';
import 'models/update_order_request.dart';
import 'myalert_dialog.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;
const LatLng DEFAULT_LOCATION = LatLng(16.842739, 96.154518);
LatLng userlocation = DEFAULT_LOCATION;
const kGoogleApiKey = "AIzaSyA18AXGZ4WtuSOa6lE2-KBQvemBgGxRjSs";
Map<PolylineId, Polyline> polylines = {};
List<LatLng> polylineCoordinates = [];
PolylinePoints polylinePoints = PolylinePoints();
// to get places detail (lat/lng)

class MyOrderDirection extends StatefulWidget {
  MyOrderDirection();
  @override
  _MyOrderDirectionState createState() => _MyOrderDirectionState();
}

class _MyOrderDirectionState extends State<MyOrderDirection> {
  _MyOrderDirectionState();

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  Marker myOrderLocationMarker;
  Map<MarkerId, PinInformation> pinInfoList = <MarkerId, PinInformation>{};
  BitmapDescriptor agentMarkerIcon;
  BitmapDescriptor myLocationMarkerIcon;
  double pinPillPosition = -100;
  CameraPosition _cameraPosition;
  PinInformation myOrderPinInfo;
  SharedPreferences prefs;
  PinInformation currentlySelectedPin = PinInformation(
      pinPath: "graphics/pinblue2.png",
      avatarPath: "graphics/user_profile.png",
      location: userlocation,
      locationName: "Current Location",
      locationDetail: "",
      distance: "",
      agentDetails: null,
      count: "",
      labelColor: Colors.black,
      isCurrentLocation: true);
  int userType = 1;
  Position userPosition;
  LatLng orderDestination;

  GoogleMapController mapController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GooglPlaces.PlacesDetailsResponse pickedPlace;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      setSourceAndDestinationIcons();
      _getUserDetails().then((value) => null);
    });
  }

  /*
  Push Messaging token: dXYummy4Qsy8mlSPD9QF5A:APA91bGWeS1CKW5Y4GJ-H1ujUAnRGCyr07nRGY4WGqDvQItyuDn_o8Bu6TKXrzDsQGMRrMwjGStwENR6JgwGmJqYP-UnU6dyaUbPabKLD_LDEetn-3tseWBqOwOEq3G-rqexbkJ2Z43Q
  */

  LoginResponse userDetail;
  String radioButtonItem;
  // Group Value for Radio Button.
  int locationType = 1;

  Future<void> _getUserDetails() async {
    prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      print("_getUserDetails  ===>" + userString);
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      userType = userDetail.data.userType;
    });
  }

  void setSourceAndDestinationIcons() async {
    agentMarkerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), "graphics/agentpin.png");
    myLocationMarkerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), "graphics/pinblue2.png");
  }

  Future<void> _getUserLocation() async {
    userPosition =
        await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (userPosition != null && orderDestination != null) {
      double distance = calculateDistance(
          userPosition.latitude,
          userPosition.longitude,
          orderDestination.latitude,
          orderDestination.longitude);
      print("Direction polylines called: distance ==>" +
          (distance * 1000).toInt().toString());
      int approxDistance = (distance * 1000).toInt();
      if (approxDistance > 1) {
        _getPolyline();
      } else {
        MyAlertDialog.showMyDialog(
            context,
            "WaterApp",
            "You have reached your destination! Please deliver your order!",
            null);
      }
    }
  }

  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    setState(() {
      mapController = controller;
      polylines = {};
      polylineCoordinates = [];
      polylinePoints = PolylinePoints();
      updateOrderLocation();
      _getUserLocation().then((value) => null);
    });
  }

  @override
  Widget build(BuildContext context) {
    final BookingDirectionParams bookingDirectionParams =
        ModalRoute.of(context).settings.arguments;
    orderDestination = new LatLng(
        double.parse(bookingDirectionParams.myOrder.latitude),
        double.parse(bookingDirectionParams.myOrder.longitude));

    _cameraPosition = CameraPosition(
      target: userlocation,
      zoom: CAMERA_ZOOM,
      bearing: CAMERA_BEARING, // 1
      tilt: CAMERA_TILT, // 2
    );
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          GoogleMap(
            myLocationEnabled: true,
            myLocationButtonEnabled: true,
            compassEnabled: true,
            tiltGesturesEnabled: true,
            markers: _markers,
            mapType: MapType.normal,
            initialCameraPosition: _cameraPosition,
            onMapCreated: _onMapCreated,
            zoomControlsEnabled: false,
            onTap: (LatLng location) {
              setState(() {
                pinPillPosition = -100;
              });
            },
            polylines: Set<Polyline>.of(polylines.values),
          ),
          AnimatedPositioned(
              bottom: pinPillPosition,
              right: 0,
              left: 0,
              duration: Duration(milliseconds: 200),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      margin: EdgeInsets.all(20),
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                blurRadius: 20,
                                offset: Offset.zero,
                                color: Colors.grey.withOpacity(0.5))
                          ]),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 40,
                                height: 40,
                                child: ClipOval(
                                    child: Image.asset(
                                        currentlySelectedPin.avatarPath,
                                        fit: BoxFit.cover))), // first widget
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(currentlySelectedPin.locationDetail,
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: currentlySelectedPin
                                                .labelColor)),
                                    if (currentlySelectedPin.agentDetails !=
                                        null)
                                      Text(
                                          "${currentlySelectedPin.agentDetails.mobileno}",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.grey)),
                                    if (currentlySelectedPin.distance != "")
                                      Text("${currentlySelectedPin.distance}",
                                          style: TextStyle(
                                              fontSize: 10, color: Colors.grey))
                                  ], // end of Column Widgets
                                ), // end of Column
                              ),
                            ), // second widget
                            Padding(
                              padding: EdgeInsets.all(15),
                              child: RaisedButton(
                                color: Colors.lightBlue,
                                textColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                padding: EdgeInsets.fromLTRB(9, 9, 9, 9),
                                splashColor: Colors.grey,
                                child: Text("Close Order"),
                                onPressed: () {
                                  print("Close Order Called");
                                  UpdateOrderRequest request =
                                      new UpdateOrderRequest(
                                          orderId:
                                              bookingDirectionParams.myOrder.id,
                                          orderStatus: true);
                                  new UserRepository()
                                      .updateOrder(request)
                                      .then((response) {
                                    if (!response.error) {
                                      Navigator.pushReplacementNamed(
                                          context, '/myorders');
                                    }
                                  });
                                },
                              ),
                            ) // third widget
                          ])) // end of Container
                  ) // end of Align
              ) // end of AnimatedPositioned
        ],
      ),
      //appBar: FloatAppBar(),
      appBar: AppBar(
        title: Text("Water Delivery"),
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        backgroundColor: Colors.deepOrange,
        content: new Text(value),
        duration: Duration(seconds: 10)));
  }

  void updateOrderLocation() async {
    _markers.remove(myOrderLocationMarker);

    myOrderLocationMarker = Marker(
        markerId: MarkerId("myLocation"),
        position: orderDestination,
        draggable: false,
        icon: agentMarkerIcon,
        onTap: () {
          setState(() {
            currentlySelectedPin = myOrderPinInfo;
            pinPillPosition = 0;
          });
        });
    _markers.add(myOrderLocationMarker);
    print("Markers Count =" + _markers.length.toString());

    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: orderDestination, zoom: 16.0),
      ),
    );
    final coordinates =
        new Coordinates(orderDestination.latitude, orderDestination.longitude);
    var addresses = await Geocoder.google(kGoogleApiKey)
        .findAddressesFromCoordinates(coordinates);

    var primaryAddress = addresses.first;
    print("Google Address ==>" + primaryAddress.toMap().toString());
    //String address =
    //      ' ${primaryAddress.locality}, ${primaryAddress.adminArea},${primaryAddress.subLocality}, ${primaryAddress.subAdminArea},${primaryAddress.addressLine}, ${primaryAddress.featureName},${primaryAddress.thoroughfare}, ${primaryAddress.subThoroughfare}';
    String shortaddress = primaryAddress.addressLine;
    String street = primaryAddress.thoroughfare;
    String division = primaryAddress.adminArea;
    String township = primaryAddress.subAdminArea;

    myOrderPinInfo = currentlySelectedPin = PinInformation(
        pinPath: "graphics/pinblue2.png",
        avatarPath: "graphics/user_profile.png",
        location: LatLng(orderDestination.latitude, orderDestination.longitude),
        locationDetail: shortaddress,
        street: street,
        division: division,
        township: township,
        distance: "",
        agentDetails: null,
        locationName: "",
        labelColor: Colors.black,
        isCurrentLocation: true);
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id, color: Colors.red, points: polylineCoordinates);
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline() async {
    if (userPosition != null) {
      PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
          kGoogleApiKey,
          PointLatLng(userPosition.latitude, userPosition.longitude),
          PointLatLng(orderDestination.latitude, orderDestination.longitude),
          travelMode: TravelMode.driving);
      if (result.points.isNotEmpty) {
        result.points.forEach((PointLatLng point) {
          polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });
      }
      _addPolyLine();
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}

class BookingDirectionParams {
  MyOrder myOrder;
  BookingDirectionParams({this.myOrder});
}

class PinInformation {
  String pinPath;
  String avatarPath;
  LatLng location;
  String locationName;
  String locationDetail;
  String township;
  String street;
  String division;
  String distance;
  Color labelColor;
  bool isCurrentLocation;
  Agent agentDetails;
  String count;
  PinInformation(
      {this.pinPath,
      this.avatarPath,
      this.location,
      this.locationName,
      this.locationDetail,
      this.township,
      this.street,
      this.division,
      this.distance,
      this.agentDetails,
      this.labelColor,
      this.isCurrentLocation,
      this.count});
}
