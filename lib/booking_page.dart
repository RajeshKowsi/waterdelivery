import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:water/models/agentmap_request.dart';
import 'package:water/models/brand_request.dart';
import 'package:water/models/brand_response.dart';
import 'package:water/models/lift_api_request.dart';
import 'package:water/models/lift_api_response.dart';
import 'package:water/models/nearbyagent_response.dart';
import 'package:water/myhomepage.dart' as HomePage;
import 'package:water/myalert_dialog.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/models/order_request.dart';
import 'package:water/models/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'common/utils.dart';

class BookingPage extends StatefulWidget {
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  TextEditingController controller = TextEditingController();
  TextEditingController agentNameController = TextEditingController();
  TextEditingController othersMobileNoController = TextEditingController();
  TextEditingController deliveryAddressController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController floorNoController = TextEditingController();

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();

  Brand selectedBrand;
  LiftData selectedFloor;

  LoginResponse userDetail;
  String mobileNumber;
  String agentMobileNumber;
  String userId;
  String agentId;
  String brandName;
  String brandId;
  int totalAmount = 0;
  int floorNo = 1;
  bool checkedValue = false;
  bool isLiftAvailable = false;
  bool isBookingForOthersChecked = false;
  bool isBookedToDefaultAgent = false;

  List<Brand> brandList = new List<Brand>();
  List<LiftData> liftNoDataList = new List<LiftData>();
  List<LiftData> liftYesDataList = new List<LiftData>();
  List<LiftData> selectedLiftDataList = new List<LiftData>();

  Future<void> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      mobileNumber = userDetail.data.mobileNumber;
      agentMobileNumber = userDetail.data.agentCode;
      agentId = userDetail.data.agentDetails.agentId;
      quantityController.text = "1";
      amountController.text = totalAmount.toString();
    });
  }

  Future<void> getBrands() async {
    print("getBrands agentNameController.text ==>" + agentNameController.text);
    new UserRepository()
        .getBrandList(new BrandRequest(mobileNumber: agentNameController.text))
        .then((brandResponse) {
      setState(() {
        brandList = brandResponse.data;
      });
    });
  }

  Future<void> getLiftNoData() async {
    print("getLiftNoData called");
    new UserRepository()
        .getLiftNoData(new LiftAPIRequest(mno: "0099"))
        .then((liftDataResponse) {
      setState(() {
        liftNoDataList = liftDataResponse.data;
        selectedLiftDataList = liftNoDataList;
        liftNoDataList.sort((a, b) => a.floorNo.compareTo(b.floorNo));
      });
    });
  }

  Future<void> getLiftData() async {
    print("getLiftNoData called");
    new UserRepository()
        .getLiftYesData(new LiftAPIRequest(mno: "0099"))
        .then((liftDataResponse) {
      setState(() {
        liftYesDataList = liftDataResponse.data;
        liftYesDataList.sort((a, b) => a.floorNo.compareTo(b.floorNo));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getUserDetails().then((value) {});
    getBrands().then((value) => null);
    getLiftNoData().then((value) => null);
    getLiftData().then((value) => null);
  }

  @override
  Widget build(BuildContext context) {
    final HomePage.BookingParams bookingAruguments =
        ModalRoute.of(context).settings.arguments;
    Agent agentDetail;
    HomePage.PinInformation bookingLocationInfo;
    int isBookingForOthers;

    if (bookingAruguments != null) {
      print("Booking Params ==>" +
          bookingAruguments.isBookingForOthers.toString());
      agentDetail = bookingAruguments.agentDetails;
      bookingLocationInfo = bookingAruguments.bookingLocationInfo;
      isBookingForOthers = bookingAruguments.isBookingForOthers;
      if (isBookingForOthers == 1) {
        isBookingForOthersChecked = true;
      }
    }

    if (agentDetail != null) {
      print("###### AgentDetail ==>" + agentDetail.mobileno);
      agentNameController.text = agentDetail.mobileno;
      agentId = agentDetail.id;
      agentMobileNumber = agentDetail.mobileno;
    } else {
      if (userDetail != null) {
        print("userDetail ==> " + userDetail.toJson().toString());
        agentMobileNumber = userDetail.data.agentCode;
        agentNameController.text = userDetail.data.agentCode;
        isBookedToDefaultAgent = true;
      }
    }

    if (bookingLocationInfo != null) {
      deliveryAddressController.text = bookingLocationInfo.locationDetail;
    }
    if (brandList == null) {
      setState(() {
        brandList = new List();
      });
    }
    if (brandList.length == 0) {
      getBrands().then((value) => {});
    }

    return new Scaffold(
        key: _keyLoader,
        appBar: AppBar(
          title: Text("Booking Page"),
        ),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Form(
            key: _formLoader,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          child: TextFormField(
                            controller: agentNameController,
                            keyboardType: TextInputType.text,
                            enabled: false,
                            decoration: InputDecoration(
                              labelText: 'Agent Code',
                              border: InputBorder.none,
                              hintText: "Agent Mobile Number",
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 10),
                              isDense: true,
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey[600],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  if (!isBookedToDefaultAgent)
                    CheckboxListTile(
                      title: Text("Save this Agent as my default"),
                      value: checkedValue,
                      onChanged: (newValue) {
                        setState(() {
                          checkedValue = newValue;
                        });
                      },
                      controlAffinity: ListTileControlAffinity
                          .leading, //  <-- leading Checkbox
                    ),
                  CheckboxListTile(
                    title: Text("Is Lift Available"),
                    value: isLiftAvailable,
                    onChanged: (value) {
                      setState(() {
                        if (isLiftAvailable != value) {
                          isLiftAvailable = value;
                          String selectedFloorNo = (selectedFloor != null)
                              ? selectedFloor.floorNo
                              : "0";
                          selectedFloor = null;
                          if (isLiftAvailable) {
                            selectedLiftDataList = liftYesDataList;
                          } else {
                            selectedLiftDataList = liftNoDataList;
                          }
                          selectedFloor = selectedLiftDataList.firstWhere(
                              (liftData) => liftData.floorNo == selectedFloorNo,
                              orElse: () => null);
                          if (selectedFloor == null) {
                            amountController.text = "0";
                          } else if (selectedFloor.floorPrice != null &&
                              selectedBrand != null) {
                            int floorPrice = (selectedFloor != null)
                                ? selectedFloor.floorPrice
                                : 1;
                            amountController.text =
                                ((selectedBrand.amt + floorPrice) *
                                        int.parse(quantityController.text))
                                    .toString();
                          } else {
                            amountController.text = "0";
                            print("Floor Details Not Valid");
                          }
                        }
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: DropdownButton<LiftData>(
                            value: selectedFloor,
                            hint: new Text("Select a floor"),
                            icon: Icon(Icons.arrow_drop_down),
                            isExpanded: true,
                            iconSize: 55,
                            elevation: 16,
                            underline: Container(
                              height: 0,
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey[600],
                            ),
                            onChanged: (LiftData liftData) {
                              setState(() {
                                selectedFloor = liftData;
                                if (liftData.floorPrice != null &&
                                    selectedBrand != null) {
                                  int floorPrice = (selectedFloor != null)
                                      ? selectedFloor.floorPrice
                                      : 1;
                                  amountController.text = ((selectedBrand.amt +
                                              floorPrice) *
                                          int.parse(quantityController.text))
                                      .toString();
                                } else {
                                  print("Floor Details Not Valid");
                                }
                              });
                            },
                            items: selectedLiftDataList
                                .map<DropdownMenuItem<LiftData>>(
                                    (LiftData liftData) {
                              return DropdownMenuItem<LiftData>(
                                  value: liftData,
                                  child: ListTile(
                                    title: Text(liftData.floorNo),
                                  ));
                            }).toList(),
                          ),
                        ))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          child: TextFormField(
                            controller: deliveryAddressController,
                            keyboardType: TextInputType.text,
                            enabled: true,
                            minLines: 2,
                            maxLines: 4,
                            decoration: InputDecoration(
                              labelText: 'Delivery Address',
                              border: InputBorder.none,
                              hintText: "Enter your full address",
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 10),
                              isDense: true,
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey[600],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: DropdownButton<Brand>(
                            value: selectedBrand,
                            hint: new Text("Select a brand"),
                            icon: Icon(Icons.arrow_drop_down),
                            isExpanded: true,
                            iconSize: 55,
                            elevation: 16,
                            underline: Container(
                              height: 0,
                            ),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.grey[600],
                            ),
                            onChanged: (Brand brand) {
                              setState(() {
                                if (brand.amt != null) {
                                  selectedBrand = brand;
                                  int floorPrice = (selectedFloor != null)
                                      ? selectedFloor.floorPrice
                                      : 1;
                                  amountController.text = ((selectedBrand.amt +
                                              floorPrice) *
                                          int.parse(quantityController.text))
                                      .toString();
                                } else {
                                  print("Brand Amount not valid");
                                }
                              });
                            },
                            items: brandList
                                .map<DropdownMenuItem<Brand>>((Brand brand) {
                              return DropdownMenuItem<Brand>(
                                  value: brand,
                                  child: ListTile(
                                    leading: Container(
                                      width: 30,
                                      height: 40,
                                      margin: EdgeInsets.only(bottom: 5),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        image: DecorationImage(
                                            image: NetworkImage(brand.imageUrl),
                                            fit: BoxFit.fill),
                                      ),
                                    ),
                                    title: Text(brand.brandName),
                                  ));
                            }).toList(),
                          ),
                        ))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          child: TextFormField(
                            controller: quantityController,
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty || value == "0") {
                                return 'Please enter number of Cans';
                              }
                              return null;
                            },
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            decoration: InputDecoration(
                              labelText: 'Quantity',
                              border: InputBorder.none,
                              hintText: "Number of can",
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 10),
                              isDense: true,
                            ),
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.grey[600],
                            ),
                            onChanged: (value) {
                              int floorPrice = (selectedFloor != null)
                                  ? selectedFloor.floorPrice
                                  : 1;
                              amountController.text =
                                  ((selectedBrand.amt + floorPrice) *
                                          int.parse(quantityController.text))
                                      .toString();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: Row(
                      children: <Widget>[
                        new Expanded(
                          child: TextFormField(
                            controller: amountController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            decoration: InputDecoration(
                              labelText: 'Amount',
                              enabled: false,
                              border: InputBorder.none,
                              hintText: "Total Price",
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 10),
                              isDense: true,
                            ),
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.grey[600],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  if (isBookingForOthers == 1)
                    CheckboxListTile(
                      title: Text("Booking For Others"),
                      value: isBookingForOthersChecked,
                      onChanged: (newValue) {
                        setState(() {
                          isBookingForOthersChecked = true;
                        });
                      },
                      controlAffinity: ListTileControlAffinity
                          .leading, //  <-- leading Checkbox
                    ),
                  if (isBookingForOthersChecked)
                    Container(
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey.withOpacity(0.5),
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      margin: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                            child: Text(
                              '09 ',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 25,
                                  fontStyle: FontStyle.italic),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          new Expanded(
                            child: TextFormField(
                              controller: othersMobileNoController,
                              keyboardType: TextInputType.text,
                              validator: (value) {
                                if (value.isEmpty || value.length < 9) {
                                  return 'Please enter valid mobile number';
                                }
                                return null;
                              },
                              enabled: true,
                              decoration: InputDecoration(
                                labelText: 'Others Mobile Number',
                                border: InputBorder.none,
                                hintText: "Mobile Number",
                                hintStyle: TextStyle(color: Colors.grey),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 10),
                                isDense: true,
                              ),
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.grey[600],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  Container(
                      height: 50,
                      padding: EdgeInsets.all(1),
                      child: RaisedButton(
                          textColor: Colors.white,
                          color: Colors.green[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.green[900])),
                          child: Text('Book Order',
                              style: TextStyle(
                                  fontSize: 20, fontStyle: FontStyle.italic)),
                          onPressed: () {
                            if (_formLoader.currentState.validate() &&
                                selectedBrand != null) {
                              //Agent Mapping Code added here

                              if (checkedValue) {
                                saveAgentAsDefault();
                              }
                              String orderMobileNumber =
                                  userDetail.data.mobileNumber;
                              String othersMobileNumber =
                                  "09" + othersMobileNoController.text;
                              String township = bookingLocationInfo.township;
                              String division = bookingLocationInfo.division;
                              OrderRequest orderRequest = new OrderRequest(
                                  mobileNumber: orderMobileNumber,
                                  otherNumber: (isBookingForOthersChecked)
                                      ? othersMobileNumber
                                      : "",
                                  userId: userDetail.data.userId,
                                  agentId: agentId,
                                  agentMobileNumber: agentNameController.text,
                                  amount: amountController.text,
                                  count: quantityController.text,
                                  brandName: selectedBrand.brandName,
                                  brandId: selectedBrand.id,
                                  address: deliveryAddressController.text,
                                  locationtype: isBookingForOthers.toString(),
                                  township: township,
                                  division: division,
                                  floorNo: floorNoController.text,
                                  latitude: bookingLocationInfo
                                      .location.latitude
                                      .toString(),
                                  longitude: bookingLocationInfo
                                      .location.longitude
                                      .toString());

                              print("booking Order Request ==>" +
                                  orderRequest.toJson().toString());
                              Utils.showLoadingDialog(context, _keyLoader);
                              new UserRepository()
                                  .placeOrder(orderRequest)
                                  .then((orderResponse) {
                                Utils.hideLoadingDialog(context);
                                MyAlertDialog.showMyDialog(
                                    context, "Water App", "Order Confirmed!",
                                    () {
                                  Navigator.pushReplacementNamed(
                                      context, '/myorders');
                                });
                              });
                            } else if (selectedBrand == null) {
                              MyAlertDialog.showMyDialog(context, "Water App",
                                  "Please select your brand", null);
                            }
                          }))
                ]),
          ),
        ));
  }

//09262888032
  Future<void> saveAgentAsDefault() async {
    AgentMapRequest request = new AgentMapRequest(
        userId: userDetail.data.userId,
        agentId: agentId,
        agentMobileNumber: agentMobileNumber,
        mobileNumber: mobileNumber);
    new UserRepository()
        .mapUserAgent(request)
        .then((response) => print("Agent Map Response =>" + response.msg));
  }
}
