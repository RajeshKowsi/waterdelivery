import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:water/models/division_response.dart';
import 'package:water/models/signup_request.dart';
import 'package:water/models/signup_response.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:device_info/device_info.dart';
import 'package:water/models/township_request.dart';
import 'package:water/models/township_response.dart';
import 'package:water/myalert_dialog.dart';
import 'package:image_picker_type/image_picker_type.dart';
import 'package:water/register_step1.dart';

import 'common/utils.dart';

class SignupPage extends StatefulWidget {
  SignupPage({Key key}) : super(key: key);
  @override
  _MySignupPageState createState() => new _MySignupPageState();
}

class _MySignupPageState extends State<SignupPage> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  final GlobalKey<State> _state = new GlobalKey<State>();
  final GlobalKey<FormState> _formLoader = new GlobalKey<FormState>();
  SignupResponse signupResponse;
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController mobileNumberController = TextEditingController();
  TextEditingController agentCodeController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController cpasswordController = TextEditingController();

  final FocusNode firstNameFocus = FocusNode();
  final FocusNode lastNameFocus = FocusNode();
  final FocusNode mobileNumberFocus = FocusNode();
  final FocusNode agentCodeFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode cpasswordFocus = FocusNode();
  //bool _autovalidate = false;
  bool _showPassword = true;

  List<Division> divisionList = new List();
  List<Township> townshipList = new List();
  Division selectedDivision;
  Township selectedTownship;

  File _image;
  int userType = 0;

  void _handleUserTypeValueChange(int value) {
    setState(() {
      userType = value;
    });
  }

  initGalleryPickUp() async {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ImagePickerHelper(
            // isSave: true,  //if you want to save image in directory
            size: Size(300, 300),
            onDone: (file) {
              if (file == null) {
                print(null);
              } else {
                setState(() {
                  _image = file;
                });
              }
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    initPlatformState().then((value) {
      setState(() {});
    });
    new UserRepository().getDivision().then((divisionResponse) {
      print("divisionList ==>" + divisionResponse.toJson().toString());
      if (!divisionResponse.error) {
        setState(() {
          divisionList = divisionResponse.data;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final OtpPageParam otpPageParam = ModalRoute.of(context).settings.arguments;
    print("otpPageParams ==>" + otpPageParam.toString());
    mobileNumberController.text = otpPageParam.mobileNumber;

    return new Scaffold(
        key: _state,
        appBar: AppBar(
          title: const Text('Sign Up Form'),
        ),
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Form(
            key: _formLoader,
            autovalidateMode: AutovalidateMode.always,
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      print("ImagePicker Should be called");
                      initGalleryPickUp();
                    },
                    child: Container(
                      width: 100,
                      height: 100,
                      margin: EdgeInsets.only(bottom: 5, top: 10),
                      decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          shape: BoxShape.circle),
                      child: new CircleAvatar(
                        backgroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        backgroundImage: (_image != null)
                            ? FileImage(_image)
                            : new AssetImage("graphics/user_profile.png"),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'First name is required field.';
                              }
                              return null;
                            },
                            focusNode: firstNameFocus,
                            controller: firstNameController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "First Name",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              firstNameFocus.unfocus();
                              if (lastNameFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(lastNameFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Last name is required field.';
                              }
                              return null;
                            },
                            focusNode: lastNameFocus,
                            controller: lastNameController,
                            keyboardType: TextInputType.text,
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Last Name",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              lastNameFocus.unfocus();
                              if (mobileNumberFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(mobileNumberFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: Text(
                            "09",
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Mobile number is required field.';
                              } else if (value.length > 10) {
                                return 'Pleae enter valid mobile number';
                              }
                              return null;
                            },
                            focusNode: mobileNumberFocus,
                            controller: mobileNumberController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            obscureText: false,
                            enabled: false,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Mobile Number",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              mobileNumberFocus.unfocus();
                              if (agentCodeFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(agentCodeFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text('I\'m a', style: new TextStyle(fontSize: 20.0)),
                      new Radio(
                        value: 1,
                        groupValue: userType,
                        onChanged: _handleUserTypeValueChange,
                      ),
                      new GestureDetector(
                        onTap: () {
                          _handleUserTypeValueChange(1);
                        },
                        child: new Text(
                          'Customer',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                      ),
                      new Radio(
                        value: 0,
                        groupValue: userType,
                        onChanged: _handleUserTypeValueChange,
                      ),
                      new GestureDetector(
                        onTap: () {
                          _handleUserTypeValueChange(0);
                        },
                        child: new Text(
                          'Agent',
                          style: new TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (userType == 1)
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey.withOpacity(0.5),
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      margin: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                      child: Row(
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15.0),
                            child: Icon(
                              Icons.people_outline,
                              color: Colors.grey,
                            ),
                          ),
                          Container(
                            height: 30.0,
                            width: 1.0,
                            color: Colors.grey.withOpacity(0.5),
                            margin:
                                const EdgeInsets.only(left: 00.0, right: 10.0),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 8),
                            child: Text(
                              "09",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          new Expanded(
                            child: TextFormField(
                              validator: (value) {
                                if (userType == 1 && value.isEmpty) {
                                  return 'Agent Mobile Number is required field.';
                                }
                                return null;
                              },
                              focusNode: agentCodeFocus,
                              controller: agentCodeController,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Agent Mobille Number",
                                hintStyle: TextStyle(color: Colors.grey),
                              ),
                              onFieldSubmitted: (value) {
                                agentCodeFocus.unfocus();
                                if (passwordFocus != null) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.location_city,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: DropdownButton<Division>(
                            value: selectedDivision,
                            hint: new Text("Select your division"),
                            isExpanded: true,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 45,
                            underline: Container(
                              height: 0,
                            ),
                            onChanged: (Division division) {
                              setState(() {
                                print("selected Division ==> " +
                                    division.toJson().toString());

                                if (selectedDivision != division) {
                                  selectedTownship = null;
                                  selectedDivision = division;
                                  print("selectedDivision 2 ==> " +
                                      selectedDivision.toJson().toString());

                                  TownshipRequest request = new TownshipRequest(
                                      divisionId: selectedDivision.id);

                                  new UserRepository()
                                      .getTownshipByDivisionId(request)
                                      .then((townshipResponse) {
                                    print("Township Response =" +
                                        townshipResponse.toJson().toString());

                                    if (!townshipResponse.error) {
                                      setState(() {
                                        townshipList = townshipResponse.data;
                                      });
                                    }
                                  });
                                }
                              });
                            },
                            items: divisionList.map<DropdownMenuItem<Division>>(
                                (Division division) {
                              return DropdownMenuItem<Division>(
                                value: division,
                                child: Container(
                                  child: new Text(division.dName),
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.location_city,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: DropdownButton<Township>(
                            value: selectedTownship,
                            hint: new Text("Select your township"),
                            isExpanded: true,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 45,
                            underline: Container(
                              height: 0,
                            ),
                            onChanged: (Township township) {
                              setState(() {
                                selectedTownship = township;
                              });
                            },
                            items: townshipList.map<DropdownMenuItem<Township>>(
                                (Township township) {
                              return DropdownMenuItem<Township>(
                                value: township,
                                child: Container(
                                  child: new Text(township.tName),
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Password is required field.';
                              }
                              return null;
                            },
                            focusNode: passwordFocus,
                            controller: passwordController,
                            keyboardType: TextInputType.text,
                            obscureText: _showPassword,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Password",
                              hintStyle: TextStyle(color: Colors.grey),
                              suffixIcon: IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: !this._showPassword
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() =>
                                      this._showPassword = !this._showPassword);
                                },
                              ),
                            ),
                            onFieldSubmitted: (value) {
                              passwordFocus.unfocus();
                              if (cpasswordFocus != null) {
                                FocusScope.of(context)
                                    .requestFocus(cpasswordFocus);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.people_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin:
                              const EdgeInsets.only(left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Password is required field.';
                              } else if (passwordController.text != value) {
                                return 'Password mis-matched';
                              }
                              return null;
                            },
                            focusNode: cpasswordFocus,
                            controller: cpasswordController,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Confirm Password",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onFieldSubmitted: (value) {
                              cpasswordFocus.unfocus();
                              _submitRegisterForm();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: FlatButton(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            splashColor: Color(0xFF4aa0d5),
                            color: Color(0xFF4aa0d5),
                            child: new Row(
                              children: <Widget>[
                                new Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    "Register",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                                new Expanded(
                                  child: Container(),
                                ),
                                new Transform.translate(
                                  offset: Offset(15.0, 0.0),
                                  child: new Container(
                                    padding: const EdgeInsets.all(5.0),
                                    child: FlatButton(
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(28.0)),
                                      splashColor: Colors.white,
                                      color: Colors.white,
                                      child: Icon(
                                        Icons.arrow_forward,
                                        color: Color(0xFF4aa0d5),
                                      ),
                                      onPressed: () {
                                        _submitRegisterForm();
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onPressed: () {
                              _submitRegisterForm();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
        ));
  }

  _submitRegisterForm() {
    if (!_formLoader.currentState.validate()) {
      return;
    }

    if (selectedDivision == null || selectedTownship == null) {
      MyAlertDialog.showMyDialog(context, "Registration Failed",
          "Please select your division and township...", null);
      return;
    }

    Utils.showLoadingDialog(context, _state);
    if (_image != null) {
      new UserRepository().uploadImage(_image.path).then((response) {
        print("Image Uploaded url ==>" + response.data);
        _submitFormData(response.data);
      });
    } else {
      _submitFormData("");
    }
  }

  _submitFormData(String profilePicUrl) {
    String firstName = firstNameController.text;
    String lastName = lastNameController.text;
    String mobileNo = "09" + mobileNumberController.text;
    String agentCode =
        (userType == 1) ? "09" + agentCodeController.text : mobileNo;
    String passWordStr = passwordController.text;
    String division = selectedDivision.dName;
    String township = selectedTownship.tName;
    //String cpass = cpasswordController.text;
    print("Device Data ==> " + _deviceData["DeviceModel"]);
    SignupRequest signupRequest = new SignupRequest(
        firstName: firstName,
        lastName: lastName,
        mobileNumber: mobileNo,
        agentCode: agentCode,
        deviceModel: _deviceData["DeviceModel"],
        deviceMake: _deviceData["DeviceMake"],
        deviceType: _deviceData["DeviceType"],
        profileImage: profilePicUrl,
        deviceUniqueId: _deviceData["DeviceUniqueId"],
        password: passWordStr,
        userType: userType.toString(),
        isActive: "1",
        division: division,
        township: township,
        latitude: "0.0",
        longitude: "0.0");

    print("SignupRequest ==> " + signupRequest.toJson().toString());

    new UserRepository().registerUser(signupRequest).then((signupResponse) {
      Utils.hideLoadingDialog(context);
      if (signupResponse.code == 200) {
        MyAlertDialog.showMyDialog(
            context, "Water Delivery", "User Registration Successful", () {
          Navigator.pop(context);
        });
      } else {
        MyAlertDialog.showMyDialog(
            context, "Registration Failed", signupResponse.msg, null);
      }
    });
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
    if (!mounted) return;
    _deviceData = deviceData;
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'DeviceMake': build.brand,
      'DeviceModel': build.device,
      'DeviceUniqueId': build.androidId,
      'DeviceType': "Android"
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'DeviceMake': data.name,
      'DeviceModel': data.model,
      'DeviceUniqueId': data.identifierForVendor,
      'DeviceType': "iOS",
    };
  }
}
