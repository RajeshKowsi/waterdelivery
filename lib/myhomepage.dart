import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:water/drawercode.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:water/bloc/user_repository.dart';
import 'package:water/map_utils.dart';
import 'package:water/models/login_response.dart';
import 'package:water/models/myorder_request.dart';
import 'package:water/models/myorder_response.dart';
import 'package:water/models/nearbyagent_request.dart';
import 'package:google_maps_webservice/places.dart' as GooglPlaces;
import 'package:water/models/nearbyagent_response.dart';
import 'package:water/models/update_user_request.dart';
import 'package:geocoder/geocoder.dart';
import 'package:water/myalert_dialog.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:water/myorders.dart';
import 'package:water/models/update_fcm_request.dart';
import 'package:geolocator/geolocator.dart';
import 'common/utils.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;
const LatLng DEFAULT_LOCATION = LatLng(16.842739, 96.154518);
LatLng userlocation = DEFAULT_LOCATION;
const kGoogleApiKey = "AIzaSyA18AXGZ4WtuSOa6lE2-KBQvemBgGxRjSs";
// to get places detail (lat/lng)
GooglPlaces.GoogleMapsPlaces _places =
    GooglPlaces.GoogleMapsPlaces(apiKey: kGoogleApiKey);

class MyHomePage extends StatefulWidget {
  MyHomePage();
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print("Notification data =" + data.toString());
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print("Notification message =" + notification.toString());
  }
  return null;

  // Or do other work.
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  _MyHomePageState();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  Marker mylocationMarker;
  Map<MarkerId, PinInformation> pinInfoList = <MarkerId, PinInformation>{};
  BitmapDescriptor agentMarkerIcon;
  BitmapDescriptor myLocationMarkerIcon;
  double pinPillPosition = -100;
  double floatingButtonPosition = 0.0;
  CameraPosition _cameraPosition;
  PinInformation agentPinInfo;
  PinInformation myLocationPinInfo;
  SharedPreferences prefs;
  PinInformation currentlySelectedPin = PinInformation(
      pinPath: "graphics/pinblue2.png",
      avatarPath: "graphics/user_profile.png",
      location: userlocation,
      locationName: "Current Location",
      locationDetail: "",
      distance: "",
      agentDetails: null,
      count: "",
      labelColor: Colors.black,
      isCurrentLocation: true);
  int userType = 1;
  bool isUserMovedTheMap = false;
  Position userPosition;

  GoogleMapController mapController;
  TextEditingController searchController = TextEditingController();
  String selectedAddress = "";
  final FocusNode searchFocus = FocusNode();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  //final GlobalKey<State> _state = new GlobalKey<State>();
  GooglPlaces.PlacesDetailsResponse pickedPlace;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    Future.delayed(Duration.zero, () {
      setSourceAndDestinationIcons();
      _getUserDetails().then((value) => null);
      initPushNotification();
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //do your stuff
      print("Home page resume called");
      if (Utils.isProfileImageUpdated) {
        print("Home page profile image updated");
        Utils.isProfileImageUpdated = false;
        _getUserDetails();
      }
    }
  }

  /*
  Push Messaging token: dXYummy4Qsy8mlSPD9QF5A:APA91bGWeS1CKW5Y4GJ-H1ujUAnRGCyr07nRGY4WGqDvQItyuDn_o8Bu6TKXrzDsQGMRrMwjGStwENR6JgwGmJqYP-UnU6dyaUbPabKLD_LDEetn-3tseWBqOwOEq3G-rqexbkJ2Z43Q
  */

  LoginResponse userDetail;
  List<MyOrder> myOrderList = new List<MyOrder>();
  String radioButtonItem;
  // Group Value for Radio Button.
  int locationType = 1;

  Future<void> _getUserDetails() async {
    prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString("userdetails");
    setState(() {
      print("_getUserDetails  ===>" + userString);
      userDetail = LoginResponse.fromJson(jsonDecode(userString));
      userType = userDetail.data.userType;
    });
  }

  Future<void> _updateFCMID(String deviceToken) async {
    UpdateFCMRequest request = new UpdateFCMRequest(
        userId: userDetail.data.userId,
        userType: userDetail.data.userType.toString(),
        fCMID: deviceToken);
    print("FCM Request =>" + request.toJson().toString());
    new UserRepository().updateFCMToken(request).then((value) {
      print("updateFCMID response : " + value.toJson().toString());
    });
  }

  void setSourceAndDestinationIcons() async {
    agentMarkerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), "graphics/agentpin.png");
    myLocationMarkerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), "graphics/pinblue2.png");
  }

  Future<void> _getUserLocation() async {
    try {
      userPosition =
          await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      double userLat, userLng;
      if (userPosition == null) {
        MyAlertDialog.showMyDialog(
            context,
            "WaterApp",
            "Unable to fetch your current location, Please check your location permission in settings...",
            null);
      } else {
        userLat = userPosition.latitude;
        userLng = userPosition.longitude;
        print("Current location  #1==> " +
            userLat.toString() +
            " : " +
            userLng.toString());
      }
      if (userDetail.data.latitude != "0.0" &&
          userDetail.data.longitude != "0.0") {
        userLat = double.parse(userDetail.data.latitude);
        userLng = double.parse(userDetail.data.longitude);
        setState(() {
          locationType = 2;
        });
      } else {
        setState(() {
          locationType = 1;
        });
        print("##### user location not set");
        if (userType == 0) {
          MyAlertDialog.showMyDialog(context, "WaterApp",
              "Dear Agent, Please save your store location to list your store to near by customers!",
              () {
            setState(() {
              pinPillPosition = 0;
              floatingButtonPosition = 80;
            });
          });
        } else {
          MyAlertDialog.showMyDialog(context, "WaterApp",
              "Dear User, Please save your delivery location to make delivery your orders!",
              () {
            setState(() {
              pinPillPosition = 0;
              floatingButtonPosition = 80;
            });
          });
        }
      }
      setState(() {
        userlocation = new LatLng(userLat, userLng);
        searchController.text = "Current Location";
      });
      print("User location #2==> " +
          userLat.toString() +
          " : " +
          userLng.toString());
      //updateUserLocationMarker(userlocation);
      pinPillPosition = -100;
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: userlocation, zoom: 16.0),
        ),
      );
    } catch (e) {
      MyAlertDialog.showMyDialog(
          context,
          "WaterApp",
          "Unable to fetch your current location, Please check your location permission in settings...",
          null);
    }
  }

  Widget _buildDialog(BuildContext context, String pushMessage) {
    return AlertDialog(
      content: Text(pushMessage),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, message["notification"]["body"]),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  void initPushNotification() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _navigateToItemDetail(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((token) {
      String fcmToken = token.toString();
      assert(fcmToken != null);
      setState(() {
        print("Push Messaging token: $fcmToken");
        _updateFCMID(fcmToken);
      });
    });
  }

  Future<void> getMyOrdersList() async {
    Utils.showLoadingDialog(context, _scaffoldKey);
    new UserRepository()
        .getMyOrderHistory(new MyOrderRequest(
            userId: userDetail.data.userId,
            userType: userDetail.data.userType.toString()))
        .then((myOrderResponse) {
      Utils.hideLoadingDialog(context);
      print("getMyOrdersList  ===>" + myOrderResponse.toJson().toString());
      if (myOrderResponse.error) {
        showInSnackBar("Unable to fetch orders, Please try again...");
        // MyAlertDialog.showMyDialog(context, "WaterApp",
        //     "Unable to fetch orders, Please try again...", null);
      } else {
        myOrderList = new List<MyOrder>();
        setState(() {
          myOrderList = myOrderResponse.data;
          if (myOrderList.length == 0) {
            showInSnackBar("No pending orders today!");
            // MyAlertDialog.showMyDialog(
            //     context, "WaterApp", "No pending orders", null);
            return;
          }

          LatLng recentOrderLocation;

          myOrderList.forEach((orderItem) {
            MarkerId marketId = MarkerId(orderItem.id);
            String infoTitle = (orderItem.brandName).toString().toUpperCase();
            double distance = 1.500;
            String distanceSuffix = " km away";
            if (distance.floor() <= 0) {
              distance = distance * 1000;
              distanceSuffix = " m away";
            }
            String count = orderItem.count.toString();
            String distanceString =
                distance.toStringAsFixed(2).toString() + distanceSuffix;
            if (recentOrderLocation == null) {
              recentOrderLocation = LatLng(
                  double.parse(orderItem.agent.latitude),
                  double.parse(orderItem.agent.longitude));
            }

            PinInformation myOrderPin = PinInformation(
                pinPath: "graphics/agentpin.png",
                avatarPath: "graphics/user_profile.png",
                location: LatLng(double.parse(orderItem.agent.latitude),
                    double.parse(orderItem.agent.longitude)),
                locationName: "",
                locationDetail: infoTitle,
                distance: "Distance: " + distanceString,
                count: "Count: " + count,
                labelColor: Colors.black,
                isCurrentLocation: false);

            pinInfoList[marketId] = myOrderPin;

            Marker resultMarker = Marker(
                markerId: marketId,
                icon: agentMarkerIcon,
                position: LatLng(double.parse(orderItem.agent.latitude),
                    double.parse(orderItem.agent.longitude)),
                onTap: () {
                  setState(() {
                    currentlySelectedPin = pinInfoList[marketId];
                    pinPillPosition = 0;
                    floatingButtonPosition = 80;
                  });
                });
            // Add it to Set
            setState(() {
              _markers.add(resultMarker);
            });
          });

          // if ((userDetail.data.latitude != "0.0" &&
          //         userDetail.data.longitude != "0.0") &&
          //     recentOrderLocation != null) {
          //   print("focus camera to recent order =>" +
          //       recentOrderLocation.toJson().toString());
          //   mapController.animateCamera(
          //     CameraUpdate.newCameraPosition(
          //       CameraPosition(target: recentOrderLocation, zoom: 16.0),
          //     ),
          //   );
          // }
        });
      }
    });
  }

  getNearByAgent(focusNearAgent) {
    Utils.showLoadingDialog(context, _scaffoldKey);
    NearByAgentRequest nearByAgentRequest = new NearByAgentRequest(
        mobileNumber: userDetail.data.mobileNumber,
        latitude: myLocationPinInfo.location.latitude.toString(),
        longitude: myLocationPinInfo.location.longitude.toString(),
        agentType: userType.toString());
    print("NearbyAgent Request ==> " + nearByAgentRequest.toJson().toString());
    new UserRepository()
        .getNearByAgent(nearByAgentRequest)
        .then((nearbyAgentRespone) {
      Utils.hideLoadingDialog(context);
      print("Nearby Agent list ===>" + nearbyAgentRespone.toJson().toString());
      if (nearbyAgentRespone.error) {
        showInSnackBar("Unable to fetch near by agents, Please try again...");
        // MyAlertDialog.showMyDialog(context, "WaterApp",
        //     "Unable to fetch near by agents, Please try again...", null);
      } else {
        List<Agent> nearbyAgents = nearbyAgentRespone.data;
        if (nearbyAgents.length == 0) {
          showInSnackBar(
              "No agents found nearby. Please try with different location.");
          // MyAlertDialog.showMyDialog(
          //     context,
          //     "WaterApp",
          //     "No agents found nearby. Please try with different location.",
          //     null);
          return;
        }
        LatLng nearbyAgentLocation;
        nearbyAgents.forEach((nearbyAgent) {
          MarkerId marketId = MarkerId(nearbyAgent.id);
          String infoTitle =
              (nearbyAgent.firstname + " " + nearbyAgent.lastname)
                  .toString()
                  .toUpperCase();
          double distance = nearbyAgent.distance;
          String distanceSuffix = " km away";
          if (distance.floor() <= 0) {
            distance = distance * 1000;
            distanceSuffix = " m away";
          }
          String mobileNo = nearbyAgent.mobileno;
          String distanceString =
              distance.toStringAsFixed(2).toString() + distanceSuffix;
          if (nearbyAgentLocation == null) {
            nearbyAgentLocation = LatLng(double.parse(nearbyAgent.latitude),
                double.parse(nearbyAgent.longitude));
          }
          PinInformation agentPin = PinInformation(
              pinPath: "graphics/agentpin.png",
              avatarPath: "graphics/user_profile.png",
              location: LatLng(double.parse(nearbyAgent.latitude),
                  double.parse(nearbyAgent.longitude)),
              locationName: mobileNo,
              locationDetail: infoTitle,
              distance: "Distance: " + distanceString,
              agentDetails: nearbyAgent,
              labelColor: Colors.black,
              isCurrentLocation: false);

          pinInfoList[marketId] = agentPin;

          Marker resultMarker = Marker(
              markerId: marketId,
              icon: agentMarkerIcon,
              position: LatLng(double.parse(nearbyAgent.latitude),
                  double.parse(nearbyAgent.longitude)),
              onTap: () {
                setState(() {
                  currentlySelectedPin = pinInfoList[marketId];
                  pinPillPosition = 0;
                  floatingButtonPosition = 80;
                });
              });
          // Add it to Set
          setState(() {
            _markers.add(resultMarker);
          });
        });
        if (focusNearAgent) {
          if ((userDetail.data.latitude != "0.0" &&
                  userDetail.data.longitude != "0.0") &&
              nearbyAgentLocation != null) {
            print("focus nearby agent ==> " +
                nearbyAgentLocation.toJson().toString());
            mapController.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(target: nearbyAgentLocation, zoom: 16.0),
              ),
            );
          }
        }

        print("Markers Count =" + _markers.length.toString());
      }
    });
  }

  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    setState(() {
      mapController = controller;
      _getUserLocation().then((value) => null);
    });
  }

  // ignore: missing_return
  Future<void> updateUserLocation(PinInformation selectedPin) {
    Utils.showLoadingDialog(context, _scaffoldKey);
    UpdateUserRequest request = new UpdateUserRequest(
        mobileNumber: userDetail.data.mobileNumber,
        latitude: currentlySelectedPin.location.latitude.toString(),
        longitude: currentlySelectedPin.location.longitude.toString(),
        address: currentlySelectedPin.locationDetail,
        street: currentlySelectedPin.street,
        division: currentlySelectedPin.division,
        township: currentlySelectedPin.township,
        userType: userType.toString());

    print("updateUserLocation ==>" + request.toJson().toString());
    new UserRepository().updateUser(request).then((response) {
      print("userRepository ==>" + response.toJson().toString());
      Utils.hideLoadingDialog(context);
      if (!response.error) {
        userDetail.data.latitude =
            currentlySelectedPin.location.latitude.toString();
        userDetail.data.longitude =
            currentlySelectedPin.location.longitude.toString();

        String userString = jsonEncode(userDetail.toJson());
        print("UserString ==>" + userString);
        prefs.setString("userdetails", userString);
        MyAlertDialog.showMyDialog(
            context, "Water App", "User Location Updated Successfully", null);
      }
    });
  }

  void _onCameraIdel() {
    // print("############ _onCameraIdel called location =" +
    //     _cameraPosition.target.latitude.toString() +
    //     ", " +
    //     _cameraPosition.target.longitude.toString());
    if (pinPillPosition != 0) {
      updateUserLocationMarker(new LatLng(
          _cameraPosition.target.latitude, _cameraPosition.target.longitude));
    }
  }

  void _onCameraMove(CameraPosition position) {
    // print("############ onCameraMove called location =" +
    //     position.target.latitude.toString() +
    //     ", " +
    //     position.target.longitude.toString());
    _cameraPosition = position;
  }

  _showSaveAdddressDialog(PinInformation selectedPin) async {
    TextEditingController addressController = new TextEditingController();
    addressController.text = selectedPin.locationDetail;
    await showDialog<String>(
      context: context,
      // ignore: deprecated_member_use
      builder: (_) => new AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                keyboardType: TextInputType.multiline,
                minLines: 2,
                maxLines: 4,
                controller: addressController,
                autofocus: true,
                decoration: new InputDecoration(
                    labelText: 'Full Address', hintText: ''),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child: const Text('SAVE'),
              onPressed: () {
                Navigator.pop(context);
                if (selectedPin.locationDetail != addressController.text) {
                  selectedPin.locationDetail = addressController.text;
                }
                updateUserLocation(selectedPin);
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _cameraPosition = CameraPosition(
      target: userlocation,
      zoom: CAMERA_ZOOM,
      bearing: CAMERA_BEARING, // 1
      tilt: CAMERA_TILT, // 2
    );

    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height - 120;
    double iconSize = 50.0;

    return WillPopScope(
      onWillPop: () => Utils.onBackPressed(context, _scaffoldKey),
      child: new Scaffold(
        key: _scaffoldKey,
        floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: floatingButtonPosition),
          child: FloatingActionButton(
            backgroundColor: Colors.white70,
            foregroundColor: Colors.blueAccent,
            onPressed: () {
              // Respond to button press
              locationType = 1;
              userlocation =
                  new LatLng(userPosition.latitude, userPosition.longitude);
              //updateUserLocationMarker(userlocation);
              setState(() {
                pinPillPosition = -100;
              });
              mapController.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(target: userlocation, zoom: 16.0),
                ),
              );
            },
            child: Icon(Icons.location_searching),
          ),
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              compassEnabled: true,
              tiltGesturesEnabled: true,
              markers: _markers,
              mapType: MapType.normal,
              initialCameraPosition: _cameraPosition,
              onMapCreated: _onMapCreated,
              onCameraMove: _onCameraMove,
              onCameraIdle: _onCameraIdel,
              zoomControlsEnabled: false,
              onTap: (LatLng location) {
                setState(() {
                  pinPillPosition = -100;
                  floatingButtonPosition = 0;
                });
              },
            ),
            new Positioned(
              top: (mapHeight - iconSize) / 2,
              right: (mapWidth - iconSize) / 2,
              child: InkWell(
                child: new Image.asset('graphics/pinblue2.png',
                    width: 50, height: 50, fit: BoxFit.fill),
                onTap: () {
                  setState(() {
                    currentlySelectedPin = myLocationPinInfo;
                    pinPillPosition = 0;
                    floatingButtonPosition = 80;
                  });
                },
              ),
            ),
            if (userType == 1)
              AnimatedPositioned(
                  top: 10,
                  right: 0,
                  left: 0,
                  duration: Duration(milliseconds: 0),
                  child: Align(
                      alignment: Alignment.topCenter,
                      child: new GestureDetector(
                        onTap: () async {
                          print("Container clicked");
                          GooglPlaces.Prediction p =
                              await PlacesAutocomplete.show(
                                  context: context,
                                  apiKey: kGoogleApiKey,
                                  mode: Mode.overlay, // Mode.fullscreen
                                  language: "en",
                                  components: [
                                new GooglPlaces.Component(
                                    GooglPlaces.Component.country, "mm")
                              ]);

                          if (p != null) {
                            print("Searched Location =" + p.placeId);
                            // get detail (lat/lng)
                            GooglPlaces.PlacesDetailsResponse detail =
                                await _places.getDetailsByPlaceId(p.placeId);
                            setState(() {
                              pickedPlace = detail;
                              final lat =
                                  pickedPlace.result.geometry.location.lat;
                              final lng =
                                  pickedPlace.result.geometry.location.lng;
                              LatLng selectedLocation = new LatLng(lat, lng);
                              locationType = 3;
                              //updateUserLocationMarker(selectedLocation);
                              pinPillPosition = -100;
                              mapController.animateCamera(
                                CameraUpdate.newCameraPosition(
                                  CameraPosition(
                                      target: selectedLocation, zoom: 16.0),
                                ),
                              );
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(1),
                          height: 90,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    blurRadius: 20,
                                    offset: Offset.zero,
                                    color: Colors.grey.withOpacity(0.5))
                              ]),
                          margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                          child: Column(
                            children: <Widget>[
                              new Expanded(
                                child: TextFormField(
                                  controller: searchController,
                                  keyboardType: TextInputType.text,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please pick your delivery address please...';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Delivery Address:',
                                    border: InputBorder.none,
                                    hintText: "Search your location",
                                    enabled: false,
                                    hintStyle: TextStyle(color: Colors.blue),
                                    suffixIcon: Icon(Icons.search),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 10),
                                    isDense: true,
                                  ),
                                  focusNode: searchFocus,
                                  onFieldSubmitted: (value) {
                                    searchFocus.unfocus();
                                  },
                                  style: TextStyle(
                                    fontSize: 10.0,
                                    color: Colors.lightGreen,
                                  ),
                                  textInputAction: TextInputAction.next,
                                ),
                              ),
                              new Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Radio(
                                      value: 1,
                                      groupValue: locationType,
                                      onChanged: (val) {
                                        setState(() {
                                          radioButtonItem = 'Current Location';
                                          locationType = 1;
                                          userlocation = new LatLng(
                                              userPosition.latitude,
                                              userPosition.longitude);
                                          //updateUserLocationMarker(
                                          //   userlocation);
                                          pinPillPosition = -100;
                                          mapController.animateCamera(
                                            CameraUpdate.newCameraPosition(
                                              CameraPosition(
                                                  target: userlocation,
                                                  zoom: 16.0),
                                            ),
                                          );
                                        });
                                      },
                                    ),
                                    Text(
                                      'Current Location',
                                      style: new TextStyle(fontSize: 10.0),
                                    ),
                                    Radio(
                                      value: 2,
                                      groupValue: locationType,
                                      onChanged: (val) {
                                        if (userDetail.data.latitude != "0.0" &&
                                            userDetail.data.longitude !=
                                                "0.0") {
                                          double userLat = double.parse(
                                              userDetail.data.latitude);
                                          double userlng = double.parse(
                                              userDetail.data.longitude);
                                          setState(() {
                                            radioButtonItem = 'My Location';
                                            locationType = 2;
                                            userlocation =
                                                new LatLng(userLat, userlng);
                                            //updateUserLocationMarker(
                                            //   userlocation);
                                            pinPillPosition = -100;
                                            mapController.animateCamera(
                                              CameraUpdate.newCameraPosition(
                                                CameraPosition(
                                                    target: userlocation,
                                                    zoom: 16.0),
                                              ),
                                            );
                                          });
                                        } else {
                                          MyAlertDialog.showMyDialog(
                                              context,
                                              "WaterApp",
                                              "Dear User, Please save your delivery location to make delivery your orders!",
                                              null);
                                        }
                                      },
                                    ),
                                    Text(
                                      'My Location',
                                      style: new TextStyle(
                                        fontSize: 10.0,
                                      ),
                                    ),
                                    Radio(
                                      value: 3,
                                      groupValue: locationType,
                                      onChanged: (val) {
                                        setState(() {
                                          radioButtonItem = 'Others';
                                          locationType = 3;
                                        });
                                      },
                                    ),
                                    Text(
                                      'Others',
                                      style: new TextStyle(
                                        fontSize: 10.0,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ))),
            AnimatedPositioned(
                bottom: pinPillPosition,
                right: 0,
                left: 0,
                duration: Duration(milliseconds: 200),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        margin: EdgeInsets.all(20),
                        height: 70,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  blurRadius: 20,
                                  offset: Offset.zero,
                                  color: Colors.grey.withOpacity(0.5))
                            ]),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(left: 10),
                                  width: 40,
                                  height: 40,
                                  child: ClipOval(
                                      child: Image.asset(
                                          currentlySelectedPin.avatarPath,
                                          fit: BoxFit.cover))), // first widget
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(currentlySelectedPin.locationDetail,
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: currentlySelectedPin
                                                  .labelColor)),
                                      if (currentlySelectedPin.agentDetails !=
                                          null)
                                        Text(
                                            "${currentlySelectedPin.agentDetails.mobileno}",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.grey)),
                                      if (currentlySelectedPin.distance != "")
                                        Text("${currentlySelectedPin.distance}",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.grey))
                                    ], // end of Column Widgets
                                  ), // end of Column
                                ),
                              ), // second widget
                              Padding(
                                padding: EdgeInsets.all(15),
                                child: RaisedButton(
                                  color: Colors.lightBlue,
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  padding: EdgeInsets.fromLTRB(9, 9, 9, 9),
                                  splashColor: Colors.grey,
                                  child: Text(
                                      (currentlySelectedPin.isCurrentLocation)
                                          ? "Save"
                                          : ((userType == 0)
                                              ? "Direction"
                                              : "Book")),
                                  onPressed: () {
                                    if (currentlySelectedPin
                                        .isCurrentLocation) {
                                      _showSaveAdddressDialog(
                                          currentlySelectedPin);
                                    } else {
                                      if (userType == 0) {
                                        MapUtils.openMap(
                                            currentlySelectedPin
                                                .location.latitude,
                                            currentlySelectedPin
                                                .location.longitude);
                                      } else {
                                        navigatToBooking();
                                      }
                                    }
                                  },
                                ),
                              ) // third widget
                            ])) // end of Container
                    ) // end of Align
                ) // end of AnimatedPositioned
          ],
        ),
        //appBar: FloatAppBar(),
        appBar: AppBar(
          title: null,
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            if (userType == 1)
              FlatButton(
                textColor: Colors.blue,
                onPressed: () {
                  navigatToBooking();
                },
                child: Text("Book"),
                shape:
                    CircleBorder(side: BorderSide(color: Colors.transparent)),
              )
          ],
        ),
        drawer: DrawerCode(userDetail: userDetail),
        //drawer: DrawerCode(),
      ),
    );
  }

  void navigatToBooking() {
    print("navigateToBooking called");
    if (userDetail.data.latitude !=
            myLocationPinInfo.location.latitude.toString() ||
        userDetail.data.longitude !=
            myLocationPinInfo.location.longitude.toString()) {
      MyAlertDialog.showLocationChangeDialog(context, "WaterApp",
          "Your selected location is different from your current location! Are you booking for others?",
          () {
        Navigator.pushNamed(context, '/booking',
            arguments: new BookingParams(
                agentDetails: currentlySelectedPin.agentDetails,
                isBookingForOthers: 1,
                bookingLocationInfo: myLocationPinInfo));
      }, () {
        Navigator.pushNamed(context, '/booking',
            arguments: new BookingParams(
                agentDetails: currentlySelectedPin.agentDetails,
                isBookingForOthers: 0,
                bookingLocationInfo: myLocationPinInfo));
      });
    } else {
      Navigator.pushNamed(context, '/booking',
          arguments: new BookingParams(
              agentDetails: currentlySelectedPin.agentDetails,
              isBookingForOthers: 0,
              bookingLocationInfo: myLocationPinInfo));
    }
  }

  void showInSnackBar(String value) {
    // _scaffoldKey.currentState.showSnackBar(new SnackBar(
    //     backgroundColor: Colors.deepOrange,
    //     content: new Text(value),
    //     duration: Duration(seconds: 10)));
  }

  void updateUserLocationMarker(LatLng location) async {
    _markers.remove(mylocationMarker);
    //mylocationMarker = null;

    // mylocationMarker = Marker(
    //     markerId: MarkerId("myLocation"),
    //     position: LatLng(location.latitude, location.longitude),
    //     draggable: false,
    //     icon: myLocationMarkerIcon,
    //     onTap: () {
    //       setState(() {
    //         currentlySelectedPin = myLocationPinInfo;
    //         pinPillPosition = 0;
    //         floatingButtonPosition = 80;
    //       });
    //     });
    // _markers.add(mylocationMarker);
    // print("Markers Count =" + _markers.length.toString());

    // mapController.animateCamera(
    //   CameraUpdate.newCameraPosition(
    //     CameraPosition(target: location, zoom: 16.0),
    //   ),
    // );
    final coordinates = new Coordinates(location.latitude, location.longitude);
    var addresses = await Geocoder.google(kGoogleApiKey)
        .findAddressesFromCoordinates(coordinates);
    var primaryAddress = addresses.first;
    print("Google Address ==>" + primaryAddress.toMap().toString());
    //String address =
    //      ' ${primaryAddress.locality}, ${primaryAddress.adminArea},${primaryAddress.subLocality}, ${primaryAddress.subAdminArea},${primaryAddress.addressLine}, ${primaryAddress.featureName},${primaryAddress.thoroughfare}, ${primaryAddress.subThoroughfare}';
    String shortaddress = primaryAddress.addressLine;
    String street = primaryAddress.thoroughfare;
    String division = primaryAddress.adminArea;
    String township = primaryAddress.subAdminArea;

    searchController.text = shortaddress;

    myLocationPinInfo = currentlySelectedPin = PinInformation(
        pinPath: "graphics/pinblue2.png",
        avatarPath: "graphics/user_profile.png",
        location: LatLng(location.latitude, location.longitude),
        locationDetail: shortaddress,
        street: street,
        division: division,
        township: township,
        distance: "",
        agentDetails: null,
        locationName: "",
        labelColor: Colors.black,
        isCurrentLocation: true);

    if (userType == 0) {
      getMyOrdersList();
    } else {
      getNearByAgent(false);
    }
  }
}

class BookingParams {
  Agent agentDetails;
  PinInformation bookingLocationInfo;
  int isBookingForOthers;
  BookingParams(
      {this.agentDetails, this.bookingLocationInfo, this.isBookingForOthers});
}

class PinInformation {
  String pinPath;
  String avatarPath;
  LatLng location;
  String locationName;
  String locationDetail;
  String township;
  String street;
  String division;
  String distance;
  Color labelColor;
  bool isCurrentLocation;
  Agent agentDetails;
  String count;
  PinInformation(
      {this.pinPath,
      this.avatarPath,
      this.location,
      this.locationName,
      this.locationDetail,
      this.township,
      this.street,
      this.division,
      this.distance,
      this.agentDetails,
      this.labelColor,
      this.isCurrentLocation,
      this.count});
}

final Map<String, Item> _items = <String, Item>{};
Item _itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['data'] ?? message;
  final String itemId = data['id'];
  final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
    ..status = data['status'];
  return item;
}

class Item {
  Item({this.itemId});
  final String itemId;

  StreamController<Item> itemController = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => itemController.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    itemController.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = '/myorders';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => MyOrdersPage(),
      ),
    );
  }
}
